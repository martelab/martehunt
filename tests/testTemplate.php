<?php

/*
testTemplate.php?q=21
    carica il template delle quest e visualizza la numero 21
    
testTemplate.php?page=final
    carica il template di final.html
    
testTemplate.php?page=removed
    carica il template di removed.html

testTemplate.php?page=wait
    carica il template di wait.html
*/


include 'test_init.php';

require_once 'log4php/LoggerManager.php';
require_once 'db/DaoManager.php';




$daoManager = DaoManager::getInstance(DAO_CONFIG_PATH);

// take quest id
if (isset($_GET["q"])) {
    $questID = (int)$_GET["q"];
    $questDataDao = $daoManager->getDao('QuestData');
    $questData = $questDataDao->selectByID($questID);
    $template = $twig->loadTemplate('quest.html');       
    // fill template with quest data and render
    echo $template->render(array("quest" => $questData));
    
}else if (isset($_GET["page"])) {
    switch($_GET["page"]) {
        case "final":
            $template = $twig->loadTemplate('final.html');
            echo $template->render(array("title" =>"Titolo",
                                         "stat" => $stat,
                                         "options"=> $options,
                                        ));   
            break;
            
        case "removed":
            $template = $twig->loadTemplate('removed.html');
            echo $template->render(array());   
            break;
            
        case "wait":
            $template = $twig->loadTemplate('wait.html');
            echo $template->render(array());   
            break;
    }
}



?>