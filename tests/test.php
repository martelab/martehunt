<?php

include 'test_init.php';

require_once 'log4php/LoggerManager.php';
require_once 'db/DaoManager.php';

$daoManager = DaoManager::getInstance(DAO_CONFIG_PATH);

///////////////////////////////////////
echo "<hr>";

echo "<h1>Team Queries</h1>";

$teamDao = $daoManager->getDao('Team');
$teams = $teamDao->selectAll();

echo "<p>" .  sizeof($teams) . " teams present </p>";


foreach($teams as $t)
{
    echo '<p style="color:#' .$t->getColor(). '">' . $t->getName() . "</p>";
}

///////////////////////////////////////
/*
echo "<hr>";

echo "<h1>Player Queries</h1>";
$playerDao = $daoManager->getDao('Player');
$players = $playerDao->selectByTeam(1);
var_dump($players);


foreach($players as $p)
{
    echo $p->getName() . '<br>';
}
*/
///////////////////////////////////////
echo "<hr>";

echo "<h1>Quests Queries</h1>";
$questDAO = $daoManager->getDao('Quest');
$quests = $questDAO->selectByID(1, 'cWClL');


//var_dump($quests);

echo "<p>" . sizeof($quests) . " quests present </p>";
echo sprintf("quest %u: %s (%s)", $quests->getId(), $quests->getName(), $quests->getPageName());
foreach($quests as $quest)
{
    echo sprintf("quest %u: %s (%s)", $quest->getId(), $quest->getName(), $quest->getPageName());
    echo "<br>";
}


?>