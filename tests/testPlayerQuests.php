<?php

include 'test_init.php';

require_once 'log4php/LoggerManager.php';
require_once 'db/DaoManager.php';

$daoManager = DaoManager::getInstance(DAO_CONFIG_PATH);

///////////////////////////////////////
echo "<hr>";

echo "<h1>PlayerQuests Queries</h1>";
$playerQuestDao = $daoManager->getDao('PlayerQuest');
//$quests = $playerQuestDao->selectByID(1, 'cWClL');
$playerQuests = $playerQuestDao->selectAll();


echo "<p>" . sizeof($playerQuests) . " playerQuests present </p>";

foreach($playerQuests as $playerQuest)
{
    echo sprintf("playerQuest %u %u %u %s %s <br>", $playerQuest->getId(), $playerQuest->getPlayerId(), $playerQuest->getQuestId(), $playerQuest->getStartTime(),  $playerQuest->getEndTime());
    echo sprintf("risposta: %s | è stato risolto? %s)", $playerQuest->getAnswer(), $playerQuest->getSolved());
    echo "<br><br>";
}

?>