<?php

include 'test_init.php';

require_once 'log4php/LoggerManager.php';
require_once 'db/DaoManager.php';

$daoManager = DaoManager::getInstance(DAO_CONFIG_PATH);

///////////////////////////////////////
echo "<hr>";

echo "<h1>Quests Queries</h1>";
$questDao = $daoManager->getDao('Quest');
$quests = $questDao->selectAll();

$questDataDao = $daoManager->getDao('QuestData');

echo "<p>" . sizeof($quests) . " quests present </p>";

foreach($quests as $quest)
{
    echo "<p>";
    echo sprintf("quest %u: %s (%s.html) secret: %s <br>", $quest->getId(), $quest->getName(), $quest->getPageName(), $quest->getSecret());
    echo sprintf("hint: %s (%s.html)", $quest->getHintSecret(), $quest->getHintPage());
 
    echo '<br><a href=http://hunt.martelab.it/d.php?l='. $quest->getId() .'&s='.$quest->getSecret().'>quest link</a>';
    echo ' | <a href=http://hunt.martelab.it/d.php?l='. $quest->getId() .'&s='.$quest->getSecret().'&a=a>answer link</a>';
 
    echo '<br><a href=d.php?l='. $quest->getId() .'&s='.$quest->getSecret().'>local quest link</a>';
    echo '| <a href=d.php?l='. $quest->getId() .'&s='.$quest->getSecret().'&a=a>local answer link</a>';
 
    
    echo "</p>";

    echo "<hr>";
}
?>