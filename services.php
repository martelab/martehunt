<?php


function createNewPlayer($playerDao, $logger)
{
  //create a new secret
  $player = new Player();
  $player->setName("unnamed");
  $player->setSecret(random_str(128));

  $player = $playerDao->insertPlayer($player);

  //store the player in the cookie
  $playerDao->toToken($player, "marteHunt");

  $logger->info( "added player " . $player->getId());
return $player;
}

function showLandingError()
{
    echo file_get_contents("templates/empty.html");
}

function showRemoval($success)
{
    if ($success) echo file_get_contents("templates/removed.html");
    else echo file_get_contents("templates/wait.html");
}

function showTeamSelection($teamDao, $twig)
{
    $teams = $teamDao->selectAll();
    $template = $twig->loadTemplate('teamSelection.html');
    echo $template->render(array("teams" => $teams));
}

function showPlayerStatus($locationDao, $player, $stat, $options, $twig)
{
    if ($options == null) $options = array();
    
    if (array_key_exists("completed", $options))
    {
      $template = $twig->loadTemplate('final.html');
      echo $template->render(array("stat" => $stat,
                                   "options"=> $options ));   
      return;
    }
    
  
    /////////////load hints
    
    $hintedLocations = $locationDao->selectHinted($player->getId());
    $solvedLocations = $locationDao->selectSolved($player->getId());

    $totLocation = $locationDao->countLocations();
    //split hinted into hint solved and hint unsolved
    $solvedMap = array();
    foreach($solvedLocations as $s)
    {
        $solvedMap[$s->getName()] = $s;
    }
        
    $solvedHints = array();    
    $unsolvedHints = array();    
    foreach($hintedLocations as $s)
    {
        if (array_key_exists($s->getName(), $solvedMap))
        {
           array_push( $solvedHints, $s);
        }
        else
        {
          array_push( $unsolvedHints, $s); 
        }
    }   

    $perc = round((100*count($solvedLocations))/$totLocation);
    
    $template = $twig->loadTemplate('playerLanding.html');
  
    echo $template->render(array("unsolvedHints" => $unsolvedHints, 
                                 "progress" => $perc, 
                                 "solvedLocations" => $solvedLocations,
                                 "stat" => $stat,
                                 "options"=> $options)
                          );   
    
 }



function parseAnswers($answers)
{
   $parsedAns = array();
   foreach($answers as $ans)
   {
     $clientAnswer = array();
     $clientAnswer["name"] = $ans["name"];
     $clientAnswer["image"] = null;
     $clientAnswer["text"] = null;
     
     if (substr( $ans["text"], 0, 7 ) == "<image>"){
       $xml = simplexml_load_string($ans["text"]);
       $image = (string) $xml[0];            
       $clientAnswer["image"] = $image;
     } else 
     {
       $clientAnswer["text"] = $ans["text"];
     }
    array_push($parsedAns, $clientAnswer);
   }
   return $parsedAns;
}


function prepareClientAnswersData($answers, $check)
{
     if (!$check["valid"])
      {
         return [];
      }
      else
      {
        //check if answers are images
        $answers2 = parseAnswers($answers);
        return $answers2 ;
      }
}
function prepareClientQuestData($quest, $check, $player, $twig)
{
        //TODO the message must be also sent in the api
        $check["message"] = "Non ancora..."; //default message
        
        //TODO the message must be also sent in the api
        //if (array_key_exists("manual", $check)  && !$check["manual"]) $check["message"] = "Devi ancora risolverlo! Torna più tardi";
        //else if (array_key_exists("dist", $check) && $check["dist"] >0) $check["message"] = "Devi avvicinarti...";
        //else if (array_key_exists("nextTime", $check) && $check["nextTime"]>0) $check["message"] = "Devi aspettare...";
   
        $quest->setAnswers( prepareClientAnswersData($quest->getAnswers(), $check));
  
        //replace quest text templates
        $template = $twig->createTemplate($quest->getQuestion());
        $quest->setQuestion($template->render(array("player" => $player)));
  
        return array("quest" => $quest, "check" => $check);
}

function showQuest($quest, $check, $player, $twig, $logger)
 {
    if (file_exists("templates/".$quest->getQuestion().".html"))
    { 
	     echo file_get_contents("templates/".$quest->getQuestion().".html");
    }
    else
    {
       $questData = prepareClientQuestData($quest, $check, $player, $twig);
       $template = $twig->loadTemplate('quest.html');
       
       // fill template with quest data and render
       echo $template->render($questData);    
    } 
 }



function processAnswer($daoManager, $locationDao, $player, $quest, $a)
{
    $result = array();
    $playerQuestDao = $daoManager->getDao("PlayerQuest");

    //insert answer
    $playerQuestDao->setAnswer($a, $player->getId(), $quest->getId());
    
    //check if was correct
    $c = $playerQuestDao->checkCorrectness($player->getId(), $quest->getId());
    $result["lastCorrect"] = $c;
    
    //get the unsolved list of location quest
    $locations = $locationDao->selectUnsolved($player->getId());
    
    if (count($locations) == 0)
    {
        $result["completed"] = true;
    }
    else 
    {
        $newLocation = $locationDao->selectNewHint($player->getId());
        
        if ($newLocation != null)
        {
            $playerHintDao = $daoManager->getDao("PlayerHint");
            $playerHintDao->insert($player->getId(), $newLocation->getId());
        }
        

    }
    return $result;
}


?>