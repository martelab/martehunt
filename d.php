<?php
include 'global_init.php';

require_once 'utils.php';
require_once 'services.php';
require_once 'log4php/LoggerManager.php';
require_once 'db/DaoManager.php';

$logger = & LoggerManager::getLogger(basename(__FILE__, '.php'));

/////////////////////////////////////////"//////////////////GET
///get requested location / secret / answer
$l = null; //location id
$s = null; //secret

$t = null; //team

//variables for answering
$q = null; //quest id
$sq = null; //quest secret
$a = null; //answer

if($_GET && isset($_GET["l"])  && isset($_GET["s"]) )
{
  $l = $_GET["l" ];
  $s = $_GET["s"];

  if (isset($_GET["t"])) $t = $_GET["t"];

  if (isset($_GET["a"])) $a = $_GET["a"];
  if (isset($_GET["sq"])) $sq = $_GET["sq"];
  if (isset($_GET["q"])) $q = $_GET["q"];
}
else
{
    showLandingError();
    die();
}

$currentTime = time();




//////////////////////////////////////////////////////////////


$daoManager = DaoManager::getInstance(DAO_CONFIG_PATH);

//1) get the user form the cookie
$playerDao = $daoManager->getDao("Player");

$player = $playerDao->fromToken("marteHunt");

if ($player == null) //let's register a new user
{
  $player = createNewPlayer($playerDao, $logger);
}
else
{
 $logger->info( $player->getId()." player " .$player->getName() ." . Welcome back");
}


if ($player->getTeamId() == null)
{
    if ($t == null)
    {
        showTeamSelection($daoManager->getDao("Team"), $twig);
		die();
	}
    else
    {
        $player = $playerDao->insertTeam($player, $t);
        $logger->info(  $player->getId()  . " player added to team " . $t);
    }
}

//get last known player positon
//$currentPos = $playerDao->getLastPosition($player->getId());
$currentPos = $player->getPosition();

//2) get the player un-resolved quests and the requested quest
$questDao = $daoManager->getDao('Quest');
$locationDao = $daoManager->getDao('Location');
$playerQuestDao = $daoManager->getDao('PlayerQuest');

/////try to get an unresolved open quest for player at this location
$quest = $questDao->selectByLocation_unresolved($l, $s, $player->getId());

if ($quest == null)
{ //try to pick a new quest for this location and assign to the player as unresolved
    $quest = $questDao->selectByLocation_new($l, $s, $player->getId());
    if ($quest != null){ //ok associate this quest to this user for this location
        $logger->info(  $player->getId() . " player opening quest " .  $quest->getId());
        $playerQuestDao->insert($player->getId(), $quest->getId());
    }
}

//3) process
$statDao = $daoManager->getDao('PlayerStat');
$stat = $statDao->selectById($player->getId());

if ($quest == null)
{ //no quest avaialble: either location was already solved or secret is wrong. Show the user current hints
   $logger->info( $player->getId()." player " .$player->getName() ." : location " .$l . " already answered or not present. Showing hints instead...");

   showPlayerStatus($locationDao, $player, $stat, null, $twig);
   die();
}

//4) Verify quest constraints prior to process the quest
$constraintsDao = $daoManager->getDao('QuestConstraints');

$questCns = $constraintsDao->select($quest->getId(), $player->getId());
$check = $questCns->check($currentTime, $currentPos, $stat->getRank());

if ($check["valid"] == false)
{
    $logger->info($player->getId() .  " player " .$player->getName() ." : location check KO " . $l);
}

//5) All is fine: process the quest and proceed
if ($a == null || ($check["valid"] == false))
{ //this is a request to see the quest
    $logger->info($player->getId() .  " player " .$player->getName() ." : show quest " . $quest->getName() . " (location ". $l.")");
    showQuest($quest, $check, $player, $twig, $logger);
    die();
}
else
{ // this is a request to insert a reply
    $logger->info($player->getId() .  " player " .$player->getName() ." : store reply to quest " . $quest->getName() . " > " . $a);
    $result = processAnswer($daoManager, $locationDao, $player, $quest, $a);

    //show results and then hints?
    $stat = $statDao->selectById($player->getId());
    showPlayerStatus($locationDao, $player, $stat, $result, $twig);
    die();
}

?>
