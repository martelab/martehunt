<?php

include 'global_init.php';

require_once 'services.php';
require_once 'log4php/LoggerManager.php';
require_once 'db/DaoManager.php';

$logger = & LoggerManager::getLogger(basename(__FILE__, '.php'));

///////////////////////////////post data


////////////////////////////////////////

///////////////////////////////get data


////////////////////////////////////////


$daoManager = DaoManager::getInstance(DAO_CONFIG_PATH);

//begin reading the config xml file
$SETUP_XML_PATH = INCLUDE_PATH . "/conf/quests.xml";

$xml = simplexml_load_file($SETUP_XML_PATH);

if (!$xml) throw new Exception("Error parsing " . $SETUP_XML_PATH);

//1) read main random seed
//$seed = $xml["url_seed"];
//srand(intval($seed));


$questDao = $daoManager->getDao("Quest");
$locationDao = $daoManager->getDao("Location");
$teamDao = $daoManager->getDao("Team");
$questConstraintDao = $daoManager->getDao("QuestConstraints");
$locationDao->removeAll(); //will also remove on cascade
$teamDao->removeAll(); //will also remove on cascade


//2) loop over all teams
$teams = array();
/*$noneTeam = new Team();
$noneTeam->setId(0); $noneTeam->setName("none");
$noneTeam->setColor("#00000000");
$teamDao->insert($noneTeam);
*/
foreach($xml->xpath('teams/team') as $teamXML) 
{ 
 $t = $teamDao->create($teamXML);
 $t = $teamDao->insert($t);
 array_push($teams, $t);
}

//3) loop over all locations
$locations = array();

foreach($xml->xpath('location') as $locationXML) 
{ 
  //create location
  $location = $locationDao->create($locationXML);

  $location = $locationDao->insert($location);
  
  array_push($locations, $location);
    
  //now get all quests
  foreach($locationXML->quest as $questXML) {
    //create quest
    $q = $questDao->create($questXML);
    $q->setLocation($location->getId());
   
    //set quest answer
    $ans = $questXML->xpath('answer');

    $answerData = array();
    foreach($ans as $a)
    {
      array_push($answerData, array("name" => (string) $a['name'], 
                                    "text" => (string) $a, 
                                    "value" => (int) $a['v']));
    }
    $q->setAnswers($answerData); 

    $questDao->insert($q);
    
    //create questConstraints
    $constraint = $questXML->xpath('constraint');
    if ($constraint != null)
    {
      $con = $questConstraintDao->create(  $constraint[0] );
      $con->setQuest($q->getId()); 
      $questConstraintDao->insert($con);
    }
    
  } 
} 

echo "Database configured using quest.xml! Start hunting now";

//3) print all location urls   
///////////////////////////////////////
echo "<hr>";
echo "<h1>Teams </h1>";

echo "<p>" . sizeof($teams) . " teams present </p>";

foreach($teams as $t)
{
 echo "<p>";
 echo sprintf("team: %s : %s", $t->getName(), $t->getColor());
 echo "</p>";
}

echo "<h1>Location Quests</h1>";
//$locations = $locationDao->selectAll();

echo "<p>" . sizeof($locations) . " locations present </p>";

foreach($locations as $l)
{
    echo "<p>";
    echo sprintf("quest %u: %s - secret: %s <br>", $l->getId(), $l->getName(), $l->getSecret());
    echo sprintf("url: href=http://www.grandecaccia.it/quest-%s&-%s", $l->getId(), $l->getSecret());
 
    echo '<br><a href=http://www.grandecaccia.it/quest-'. $l->getId() .'-'.$l->getSecret().'>quest link</a>';
    echo ' | <a href=http://www.grandecaccia.it/quest-'. $l->getId() .'-'.$l->getSecret().'&a=a>answer link</a>';
 
    echo '<br><a href=d.php?l='. $l->getId() .'&s='.$l->getSecret().'>local quest link</a>';
    echo '| <a href=d.php?l='. $l->getId() .'&s='.$l->getSecret().'&a=a>local answer link</a>';
 
    
    echo "</p>";

    echo "<hr>";
}

echo "<p>";
echo "<h3>List of links</h3>";
foreach($locations as $l)
{
   echo sprintf("http://www.grandecaccia.it/quest-%s-%s<br>", $l->getId(), $l->getSecret());
}
echo "</p>";
?>