<?php

include 'service_init.php';

require_once 'log4php/LoggerManager.php';
require_once 'db/DaoManager.php';
require_once '../services.php';

$logger = & LoggerManager::getLogger(basename(__FILE__, '.php'));


if($_GET && isset($_GET["l"])  && isset($_GET["s"]) ) 
{
  $l = $_GET["l" ];
  $s = $_GET["s"];
} else
{
	jsonReply([]);
}
$currentTime = time();



$xml = simplexml_load_file(CONFIG_PATH);
$storePositions = $xml->xpath('positioning/storage')[0];
         
$daoManager = DaoManager::getInstance(DAO_CONFIG_PATH);

$playerDao = $daoManager->getDao("Player");
$player = $playerDao->fromToken("marteHunt");

if ($player == null)
{
	jsonReply([]);
}
else
{
 $logger->info( $player->getId()." player  " .$player->getName() ."  ask quest services");	
}


$currentPos = $player->getPosition();

if ($storePositions)
{
$logger->info( $player->getId()." player storing pos " .$player->getPosition()[0] ."  " .$player->getPosition()[1] );	
  $playerDao->updatePlayerPosition($player);
}



/////try to get an unresolved open quest for player at this location
$questDao = $daoManager->getDao('Quest');

$quest = $questDao->selectByLocation_unresolved($l, $s, $player->getId());        
if ($quest == null)
{
	jsonReply([]);
}


$statDao = $daoManager->getDao('PlayerStat');
$stat = $statDao->selectById($player->getId());

$constraintsDao = $daoManager->getDao('QuestConstraints');
$questCns = $constraintsDao->select($quest->getId(), $player->getId());
$check = $questCns->check($currentTime, $currentPos, $stat->getRank());

if ($check["valid"] == false)
{
    //$check["latlon_debug"] = $player->getPosition();
    $logger->info($player->getId() .  " player  " .$player->getName() ." : quest check KO " . $quest->getName());
	jsonReply($check);
} else 
{
  //send list of answers
  $clientAnswers = prepareClientAnswersData($quest->getAnswers(), $check);
  $check["answers"] = $clientAnswers;
  jsonReply($check);
}


?>