<?php

date_default_timezone_set("Europe/Rome");

if (! defined('INCLUDE_PATH')) {
	define('INCLUDE_PATH', dirname(__FILE__) . "/../" );
	set_include_path(get_include_path() . PATH_SEPARATOR . INCLUDE_PATH);
    set_include_path(get_include_path() . PATH_SEPARATOR . INCLUDE_PATH . "libs/");
}

define('LOCALE_SUFFIX', 'it');

// set bundles
define('BUNDLE_PATH', INCLUDE_PATH . '/conf/properties');
define('DEFAULT_BUNDLE', BUNDLE_PATH . '/default.properties');
define('MAIL_BUNDLE', BUNDLE_PATH . '/mail.properties');

define('CONFIG_PATH', INCLUDE_PATH . '/conf/config.xml');
define('DAO_CONFIG_PATH', INCLUDE_PATH . '/conf/config.xml');
define('CACHE_CONFIG_PATH', INCLUDE_PATH . '/conf/cache-config.xml');
define('LOG4PHP_CONFIGURATION', INCLUDE_PATH . '/conf/log4php.xml');
define('LOGS_PATH', INCLUDE_PATH . '/logs');

//include Components auto loading
require 'vendor/autoload.php';


// exception handler
require_once 'messages/ExceptionHandler.php';


////////// ENABLING ASSERTION
assert_options(ASSERT_ACTIVE, true);
assert_options(ASSERT_BAIL, true);

//////////global methods
function jsonReply($array)
{
    $ts = gmdate("D, d M Y H:i:s") . " GMT";  
    header("Expires: $ts");
    header("Last-Modified: $ts");
    header("Cache-Control: no-cache, must-revalidate");
    header("Content-Type: application/json;charset=utf-8");
    echo json_encode($array);
	die();
}
// fill response with message provided by messenger
function fillMessages(Response $response, Messenger $messenger, User $user = null) {
	
	$messageConverter = new MessageConverter();
	if ($user != null) {
		$response->addMessages($messageConverter->convertToClientMessages(retrieveNotifications($user)));
	}
	
	// add execution messages
	$response->addMessages($messageConverter->convertToClientMessages($messenger->getMessages()));
	
	if (! $messenger->getMessages()->getMessagesOfType(MessageType::ERROR())->isEmpty()) {
		$response->setError(true);
	}
}

// fill response with message provided by messenger
// $user is provided only if service require authentication
function manageError(Response $response, Exception $e, User $user = null) {
	
	$messageConverter = new MessageConverter();
	
	if ($user != null) {
		$response->addMessages($messageConverter->convertToClientMessages(retrieveNotifications($user)));
	}
	
	$response->setError(true);
	
	if ($e instanceof MessageException) {
		$response->addMessage($messageConverter->convertToClientMessage($e->getMessage()));
	} else {
		$response->addMessage(MessageConverter::createDefaultErrorMessage());
	}

}


?>