<?php

include 'service_init.php';

require_once 'log4php/LoggerManager.php';
require_once 'db/DaoManager.php';

$daoManager = DaoManager::getInstance(DAO_CONFIG_PATH);


$teamDao = $daoManager->getDao('Team');
$teams = $teamDao->selectAll_score();

$statDao = $daoManager->getDao('PlayerStat');
$stats = $statDao->selectRegistered_score();
     
$response = array("teamRank" => $teams, 
                  "playerRank" => $stats );

jsonReply($response);

?>