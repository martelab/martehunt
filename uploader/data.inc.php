<?php
$bitbucket_credentials = array(
    'username' => '[GIT_USER]',
    'password' => '[GIT_PASSWORD]'
);

$projects = array(
	array(
		'git_slug'=>'martehunt',
		'branches'=>array(
			'master'=>array(
				'type'=>'ftp',
				'ftp_host'=>'[SERVER_IP]',
				'ftp_user'=>'[FTP_USER]',
				'ftp_pass'=>'[FTP_PASSWORD]',
				'ftp_path'=>'/dev/',
			),

                        'release'=>array(
				'type'=>'ftp',
				'ftp_host'=>'[SERVER_IP]',
				'ftp_user'=>'[FTP_USER]',
				'ftp_pass'=>'[FTP_PASSWORD]',
				'ftp_path'=>'/release/',
			),
		),
	),
);
?>
