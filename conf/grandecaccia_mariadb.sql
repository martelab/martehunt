-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 29, 2018 at 09:19 PM
-- Server version: 10.1.26-MariaDB-0+deb9u1
-- PHP Version: 7.0.30-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gorgonzola2018`
--

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE `answer` (
  `id` int(11) NOT NULL,
  `quest` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `answerstats_count`
-- (See below for the actual view)
--
CREATE TABLE `answerstats_count` (
`quiestId` int(11)
,`answerId` int(11)
,`nome` varchar(255)
,`ans` varchar(255)
,`text` text
,`value` int(11)
,`num` bigint(21)
);

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `secret` varchar(255) NOT NULL,
  `hintText` text NOT NULL,
  `type` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `locationevents`
-- (See below for the actual view)
--
CREATE TABLE `locationevents` (
`location` int(11)
,`name` varchar(255)
,`started` timestamp
,`ended` timestamp
,`elapsed` time
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `locationstats`
-- (See below for the actual view)
--
CREATE TABLE `locationstats` (
`name` varchar(255)
,`type` tinytext
,`found` bigint(21)
,`suggested` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `locationstats_answers`
-- (See below for the actual view)
--
CREATE TABLE `locationstats_answers` (
`location` varchar(255)
,`correct_answer` decimal(23,0)
,`num_answer` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `locationstats_complete`
-- (See below for the actual view)
--
CREATE TABLE `locationstats_complete` (
`name` varchar(255)
,`type` tinytext
,`suggested` bigint(21)
,`found` bigint(21)
,`found_and_suggested` bigint(21)
,`correct_answers` decimal(23,0)
,`num_answers` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `locationstats_difficulty`
-- (See below for the actual view)
--
CREATE TABLE `locationstats_difficulty` (
`location` varchar(255)
,`domanda` decimal(27,4)
,`suggerimento` decimal(24,4)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `locationstats_suggested`
-- (See below for the actual view)
--
CREATE TABLE `locationstats_suggested` (
`location` varchar(255)
,`found_and_suggested` bigint(21)
);

-- --------------------------------------------------------

--
-- Table structure for table `manual_constraint_resolution`
--

CREATE TABLE `manual_constraint_resolution` (
  `questConstraint` int(11) NOT NULL,
  `player` int(11) NOT NULL,
  `resolved` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `player`
--

CREATE TABLE `player` (
  `id` int(11) NOT NULL,
  `name` varchar(11) CHARACTER SET utf8 NOT NULL,
  `active` int(11) NOT NULL,
  `secret` varchar(255) CHARACTER SET utf8 NOT NULL,
  `teamId` int(11) NOT NULL,
  `sprite` int(11) DEFAULT NULL,
  `color` varchar(11) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `playeranswerscore`
--

CREATE TABLE `playeranswerscore` (
  `playerId` int(11) DEFAULT NULL,
  `teamId` int(11) DEFAULT NULL,
  `answerPoints` decimal(32,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Stand-in structure for view `playercount`
-- (See below for the actual view)
--
CREATE TABLE `playercount` (
`t` bigint(21)
);

-- --------------------------------------------------------

--
-- Table structure for table `playerhint`
--

CREATE TABLE `playerhint` (
  `id` int(11) NOT NULL,
  `playerId` int(11) NOT NULL,
  `locationId` int(11) NOT NULL,
  `startTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `endTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Stand-in structure for view `playerlocationscore`
-- (See below for the actual view)
--
CREATE TABLE `playerlocationscore` (
`playerId` int(11)
,`locationPoints` decimal(23,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `playerpoints`
-- (See below for the actual view)
--
CREATE TABLE `playerpoints` (
`playerID` int(11)
,`teamID` int(11)
,`points` decimal(33,0)
,`elapsed` decimal(20,0)
);

-- --------------------------------------------------------

--
-- Table structure for table `playerposition`
--

CREATE TABLE `playerposition` (
  `id` int(11) NOT NULL,
  `playerId` int(11) NOT NULL,
  `latlon` point NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `playerquest`
--

CREATE TABLE `playerquest` (
  `id` int(11) NOT NULL,
  `playerId` int(11) NOT NULL,
  `questId` int(11) NOT NULL,
  `startTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `endTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `answer` varchar(11) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `playerrank`
-- (See below for the actual view)
--
CREATE TABLE `playerrank` (
`playerId` int(11)
,`points` decimal(33,0)
,`rank` bigint(22)
,`percentile` decimal(27,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `playerrank_`
-- (See below for the actual view)
--
CREATE TABLE `playerrank_` (
`playerId` int(11)
,`points` decimal(33,0)
,`rank` bigint(22)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `playerscore`
-- (See below for the actual view)
--
CREATE TABLE `playerscore` (
`playerId` int(11)
,`teamId` int(11)
,`answerPoints` decimal(32,0)
,`correct` decimal(23,0)
,`wrong` decimal(23,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `playertime`
-- (See below for the actual view)
--
CREATE TABLE `playertime` (
`playerId` int(11)
,`elapsedTime` bigint(21)
,`maxTime` timestamp
,`minTime` timestamp
);

-- --------------------------------------------------------

--
-- Table structure for table `quest`
--

CREATE TABLE `quest` (
  `id` int(11) NOT NULL,
  `location` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `secret` varchar(255) CHARACTER SET utf8 NOT NULL,
  `question` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `questconstraints`
--

CREATE TABLE `questconstraints` (
  `id` int(11) NOT NULL,
  `questId` int(11) NOT NULL,
  `posConstraint` tinyint(1) NOT NULL,
  `pos` point NOT NULL,
  `distance` float NOT NULL,
  `manualConstraint` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Stand-in structure for view `questevents`
-- (See below for the actual view)
--
CREATE TABLE `questevents` (
`player` int(11)
,`quest` int(11)
,`location` int(11)
,`started` timestamp
,`ended` timestamp
,`elapsed` time
);

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `color` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure for view `answerstats_count`
--
DROP TABLE IF EXISTS `answerstats_count`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `answerstats_count`  AS  select `q`.`id` AS `quiestId`,`a`.`id` AS `answerId`,`q`.`name` AS `nome`,`a`.`name` AS `ans`,`a`.`text` AS `text`,`a`.`value` AS `value`,count(0) AS `num` from ((`playerquest` `pq` join `quest` `q` on((`q`.`id` = `pq`.`questId`))) join `answer` `a` on(((`q`.`id` = `a`.`quest`) and (convert(`a`.`name` using utf8) = `pq`.`answer`)))) group by `q`.`name`,`a`.`name` ;

-- --------------------------------------------------------

--
-- Structure for view `locationevents`
--
DROP TABLE IF EXISTS `locationevents`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `locationevents`  AS  select `l`.`id` AS `location`,`l`.`name` AS `name`,`qe`.`started` AS `started`,`qe`.`ended` AS `ended`,`qe`.`elapsed` AS `elapsed` from (`questevents` `qe` join `location` `l` on((`l`.`id` = `qe`.`location`))) order by `qe`.`started` ;

-- --------------------------------------------------------

--
-- Structure for view `locationstats`
--
DROP TABLE IF EXISTS `locationstats`;

CREATE ALGORITHM=UNDEFINED DEFINER=`martelab`@`localhost` SQL SECURITY DEFINER VIEW `locationstats`  AS  select `l`.`name` AS `name`,`l`.`type` AS `type`,(select count(0) from (`playerquest` `pq` join `quest` `q2` on((`q2`.`id` = `pq`.`questId`))) where (`q2`.`location` = `l`.`id`)) AS `found`,(select count(0) from `playerhint` `ph` where (`ph`.`locationId` = `l`.`id`)) AS `suggested` from (`quest` `q` join `location` `l` on((`q`.`location` = `l`.`id`))) group by `l`.`id` order by (select count(0) from (`playerquest` `pq` join `quest` `q2` on((`q2`.`id` = `pq`.`questId`))) where (`q2`.`location` = `l`.`id`)) desc ;

-- --------------------------------------------------------

--
-- Structure for view `locationstats_answers`
--
DROP TABLE IF EXISTS `locationstats_answers`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `locationstats_answers`  AS  select `l`.`name` AS `location`,sum((case when (`a`.`value` > 0) then 1 else 0 end)) AS `correct_answer`,count(0) AS `num_answer` from (((`playerquest` `pq` join `answer` `a` on(((convert(`a`.`name` using utf8) = `pq`.`answer`) and (`a`.`quest` = `pq`.`questId`)))) join `quest` `q` on((`q`.`id` = `pq`.`questId`))) join `location` `l` on((`l`.`id` = `q`.`location`))) group by `l`.`name` order by count(0) ;

-- --------------------------------------------------------

--
-- Structure for view `locationstats_complete`
--
DROP TABLE IF EXISTS `locationstats_complete`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `locationstats_complete`  AS  select `locationstats`.`name` AS `name`,`locationstats`.`type` AS `type`,`locationstats`.`suggested` AS `suggested`,`locationstats`.`found` AS `found`,`locationstats_suggested`.`found_and_suggested` AS `found_and_suggested`,`locationstats_answers`.`correct_answer` AS `correct_answers`,`locationstats_answers`.`num_answer` AS `num_answers` from ((`locationstats` join `locationstats_suggested` on((`locationstats`.`name` = `locationstats_suggested`.`location`))) join `locationstats_answers` on((`locationstats`.`name` = `locationstats_answers`.`location`))) ;

-- --------------------------------------------------------

--
-- Structure for view `locationstats_difficulty`
--
DROP TABLE IF EXISTS `locationstats_difficulty`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `locationstats_difficulty`  AS  select `lc`.`name` AS `location`,(`lc`.`correct_answers` / `lc`.`num_answers`) AS `domanda`,(`lc`.`found_and_suggested` / `lc`.`suggested`) AS `suggerimento` from `locationstats_complete` `lc` ;

-- --------------------------------------------------------

--
-- Structure for view `locationstats_suggested`
--
DROP TABLE IF EXISTS `locationstats_suggested`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `locationstats_suggested`  AS  select `l`.`name` AS `location`,count(0) AS `found_and_suggested` from (((`location` `l` join `playerhint` `h` on((`h`.`locationId` = `l`.`id`))) join `playerquest` `pq` on((`h`.`playerId` = `pq`.`playerId`))) join `quest` `q` on(((`pq`.`questId` = `q`.`id`) and (`q`.`location` = `l`.`id`)))) group by `l`.`name` ;

-- --------------------------------------------------------

--
-- Structure for view `playercount`
--
DROP TABLE IF EXISTS `playercount`;

CREATE ALGORITHM=UNDEFINED DEFINER=`martelab`@`localhost` SQL SECURITY DEFINER VIEW `playercount`  AS  select count(0) AS `t` from `player` ;

-- --------------------------------------------------------

--
-- Structure for view `playerlocationscore`
--
DROP TABLE IF EXISTS `playerlocationscore`;

CREATE ALGORITHM=UNDEFINED DEFINER=`martelab`@`localhost` SQL SECURITY DEFINER VIEW `playerlocationscore`  AS  select `p`.`id` AS `playerId`,sum((case when (`pq`.`id` is not null) then 1 else 0 end)) AS `locationPoints` from (`player` `p` left join `playerquest` `pq` on((`pq`.`playerId` = `p`.`id`))) group by `p`.`id` ;

-- --------------------------------------------------------

--
-- Structure for view `playerpoints`
--
DROP TABLE IF EXISTS `playerpoints`;

CREATE ALGORITHM=UNDEFINED DEFINER=`martelab`@`localhost` SQL SECURITY DEFINER VIEW `playerpoints`  AS  select `pa`.`playerId` AS `playerID`,`pa`.`teamId` AS `teamID`,(`pa`.`answerPoints` + `pl`.`locationPoints`) AS `points`,ifnull(`pt`.`elapsedTime`,0) AS `elapsed` from ((`playerscore` `pa` join `playerlocationscore` `pl` on((`pa`.`playerId` = `pl`.`playerId`))) left join `playertime` `pt` on((`pa`.`playerId` = `pt`.`playerId`))) ;

-- --------------------------------------------------------

--
-- Structure for view `playerrank`
--
DROP TABLE IF EXISTS `playerrank`;

CREATE ALGORITHM=UNDEFINED DEFINER=`martelab`@`localhost` SQL SECURITY DEFINER VIEW `playerrank`  AS  select `r1`.`playerId` AS `playerId`,`r1`.`points` AS `points`,`r1`.`rank` AS `rank`,round(((((`r2`.`t` - `r1`.`rank`) + 1) * 100) / `r2`.`t`),0) AS `percentile` from (`playerrank_` `r1` join `playercount` `r2`) ;

-- --------------------------------------------------------

--
-- Structure for view `playerrank_`
--
DROP TABLE IF EXISTS `playerrank_`;

CREATE ALGORITHM=UNDEFINED DEFINER=`martelab`@`localhost` SQL SECURITY DEFINER VIEW `playerrank_`  AS  select `pc1`.`playerID` AS `playerId`,`pc1`.`points` AS `points`,(count(`pc2`.`points`) + 1) AS `rank` from (`playerpoints` `pc1` left join `playerpoints` `pc2` on(((`pc1`.`points` < `pc2`.`points`) or ((`pc1`.`points` = `pc2`.`points`) and (`pc1`.`elapsed` > `pc2`.`elapsed`))))) group by `pc1`.`playerID` ;

-- --------------------------------------------------------

--
-- Structure for view `playerscore`
--
DROP TABLE IF EXISTS `playerscore`;

CREATE ALGORITHM=UNDEFINED DEFINER=`martelab`@`localhost` SQL SECURITY DEFINER VIEW `playerscore`  AS  select `p`.`id` AS `playerId`,`p`.`teamId` AS `teamId`,sum((case when (`a`.`value` is not null) then `a`.`value` else 0 end)) AS `answerPoints`,sum((case when ((`a`.`value` > 0) and (`a`.`value` is not null)) then 1 else 0 end)) AS `correct`,sum((case when ((`a`.`value` = 0) and (`a`.`value` is not null)) then 1 else 0 end)) AS `wrong` from ((`player` `p` left join `playerquest` `pq` on((`pq`.`playerId` = `p`.`id`))) left join `answer` `a` on(((`a`.`quest` = `pq`.`questId`) and (convert(`a`.`name` using utf8) = `pq`.`answer`)))) group by `p`.`id` ;

-- --------------------------------------------------------

--
-- Structure for view `playertime`
--
DROP TABLE IF EXISTS `playertime`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `playertime`  AS  select `pq`.`playerId` AS `playerId`,timestampdiff(SECOND,min(`pq`.`startTime`),max(`pq`.`endTime`)) AS `elapsedTime`,max(`pq`.`endTime`) AS `maxTime`,min(`pq`.`startTime`) AS `minTime` from `playerquest` `pq` group by `pq`.`playerId` ;

-- --------------------------------------------------------

--
-- Structure for view `questevents`
--
DROP TABLE IF EXISTS `questevents`;

CREATE ALGORITHM=UNDEFINED DEFINER=`martelab`@`localhost` SQL SECURITY DEFINER VIEW `questevents`  AS  select `pq`.`playerId` AS `player`,`q`.`id` AS `quest`,`l`.`id` AS `location`,`pq`.`startTime` AS `started`,`pq`.`endTime` AS `ended`,timediff(`pq`.`endTime`,`pq`.`startTime`) AS `elapsed` from ((`playerquest` `pq` join `quest` `q` on((`q`.`id` = `pq`.`questId`))) join `location` `l` on((`l`.`id` = `q`.`location`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answer`
--
ALTER TABLE `answer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quest` (`quest`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `manual_constraint_resolution`
--
ALTER TABLE `manual_constraint_resolution`
  ADD PRIMARY KEY (`questConstraint`,`player`),
  ADD KEY `questConstraint` (`questConstraint`),
  ADD KEY `player` (`player`);

--
-- Indexes for table `player`
--
ALTER TABLE `player`
  ADD PRIMARY KEY (`id`),
  ADD KEY `teamId` (`teamId`) USING BTREE;

--
-- Indexes for table `playerhint`
--
ALTER TABLE `playerhint`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questId` (`locationId`),
  ADD KEY `playerId` (`playerId`);

--
-- Indexes for table `playerposition`
--
ALTER TABLE `playerposition`
  ADD PRIMARY KEY (`id`),
  ADD KEY `playerId` (`playerId`),
  ADD KEY `playerId_2` (`playerId`);

--
-- Indexes for table `playerquest`
--
ALTER TABLE `playerquest`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questId` (`questId`),
  ADD KEY `playerId` (`playerId`);

--
-- Indexes for table `quest`
--
ALTER TABLE `quest`
  ADD PRIMARY KEY (`id`),
  ADD KEY `location` (`location`) USING BTREE;

--
-- Indexes for table `questconstraints`
--
ALTER TABLE `questconstraints`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questId` (`questId`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answer`
--
ALTER TABLE `answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `player`
--
ALTER TABLE `player`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `playerhint`
--
ALTER TABLE `playerhint`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `playerposition`
--
ALTER TABLE `playerposition`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `playerquest`
--
ALTER TABLE `playerquest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `quest`
--
ALTER TABLE `quest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `questconstraints`
--
ALTER TABLE `questconstraints`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `answer`
--
ALTER TABLE `answer`
  ADD CONSTRAINT `answer_ibfk_1` FOREIGN KEY (`quest`) REFERENCES `quest` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `manual_constraint_resolution`
--
ALTER TABLE `manual_constraint_resolution`
  ADD CONSTRAINT `manual_constraint_resolution_ibfk_1` FOREIGN KEY (`questConstraint`) REFERENCES `questconstraints` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `manual_constraint_resolution_ibfk_2` FOREIGN KEY (`player`) REFERENCES `playerquest` (`playerId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `playerhint`
--
ALTER TABLE `playerhint`
  ADD CONSTRAINT `playerhint_ibfk_1` FOREIGN KEY (`locationId`) REFERENCES `location` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `playerhint_ibfk_2` FOREIGN KEY (`playerId`) REFERENCES `player` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `playerposition`
--
ALTER TABLE `playerposition`
  ADD CONSTRAINT `playerPosition_ibfk_1` FOREIGN KEY (`playerId`) REFERENCES `player` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `playerquest`
--
ALTER TABLE `playerquest`
  ADD CONSTRAINT `playerquest_ibfk_1` FOREIGN KEY (`playerId`) REFERENCES `player` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `playerquest_ibfk_2` FOREIGN KEY (`questId`) REFERENCES `quest` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `quest`
--
ALTER TABLE `quest`
  ADD CONSTRAINT `quest_ibfk_1` FOREIGN KEY (`location`) REFERENCES `location` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `questconstraints`
--
ALTER TABLE `questconstraints`
  ADD CONSTRAINT `questConstraints_ibfk_1` FOREIGN KEY (`questId`) REFERENCES `quest` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
