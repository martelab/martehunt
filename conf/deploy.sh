#install required applications
apt update

#install lamp
apt-get install apache2
apt-get install mysql-server
mysql_secure_installation

apt-get install php

service apache2 restart




apt install curl
apt install composer

apt update
#apt install npm : not working. We download the correct script
cd ~
curl -sL https://deb.nodesource.com/setup_6.x -o nodesource_setup.sh
bash nodesource_setup.sh
apt-get install nodejs

npm install -g bower

#move to the webpage folder
mkdir -p /var/www/html/gc_fragolosa
cd /var/www/html/gc_fragolosa

#clone the current repository
git clone --depth 1 https://bitbucket.org/martelab/grandecaccia2016.git .

#install dependecies
mkdir libs
mkdir js
mkdir vendor
mkdir templates/compilation_cache
chmod 777 templates/compilation_cache

bower install --allow-root
composer update
git clone --depth 1 https://bitbucket.org/nemedian/nemediandao.git libs


#change owner 
chown -R  martelab:martelab .

#create database and user
mysql -u root -p -e "create database fragolosa2018; GRANT ALL PRIVILEGES ON fragolosa2018.* TO martelab@localhost IDENTIFIED BY 'Martesana Rulez'"
mysql -u root -p -e "GRANT ALL PRIVILEGES ON fragolosa2018.* TO phpmyadmin@localhost; GRANT SUPER ON *.* TO phpmyadmin@localhost;"

mysql -u root -p -e "SET GLOBAL time_zone = '+2:00';"
#upload inital database script
mysql -u phpmyadmin -p fragolosa < conf/martelab_mariadb.sql


#prepare https
apt-get install python-certbot-apache

#request a new certificate from letsencrypt. Use as webroot /var/www/html/
certbot --authenticator webroot --installer apache

#cd /etc/apache2/
#mkdir ssl
#cd ssl
#mkdir crt
#mkdir key
#openssl req -new -x509 -days 100 -keyout key/grandecaccia.key -out crt/grandecaccia.crt -nodes -subj  "/O=Martelab/OU=Grande Caccia/CN=dev.martelab.it"


#copy apache2 virtual server
cp conf/*.conf /etc/apache2/sites-available
replace "gc_dev" "gc_fragolosa" -- /etc/apache2/sites-available/*.conf

#enable virtual server
a2enmod rewrite 
a2ensite 001-grandecaccia.conf
a2dissite 000-default.conf
systemctl reload apache2

