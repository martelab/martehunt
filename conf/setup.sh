sudo sudo

#activate certificates
certbot --apache 
#make sure only http is enabled and no SSL istruction are present in Location

#update certificates
certbot renew

#enable base site in /etc/apache2/sites-available/
a2ensite 000-kids.martelab.it.conf
a2ensite 000-dev.martelab.it.conf

systemctl reload apache2


#check apache status
sudo systemctl status apache2