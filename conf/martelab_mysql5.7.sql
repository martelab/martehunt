-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 24, 2017 at 03:34 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE `answer` (
  `id` int(11) NOT NULL,
  `quest` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `secret` varchar(255) NOT NULL,
  `hintText` text NOT NULL,
  `type` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
--
-- Stand-in structure for view `locationEvents`
--
DROP VIEW IF EXISTS `locationEvents`;
CREATE TABLE `locationEvents` (
`name` varchar(255)
,`started` timestamp
,`ended` timestamp
,`elapsed` time
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `locationstats`
--
DROP VIEW IF EXISTS `locationstats`;
CREATE TABLE `locationstats` (
`name` varchar(255)
,`type` tinytext
,`found` bigint(21)
,`suggested` bigint(21)
);

-- --------------------------------------------------------

--
-- Table structure for table `player`
--

CREATE TABLE `player` (
  `id` int(11) NOT NULL,
  `name` varchar(11) CHARACTER SET utf8 NOT NULL,
  `active` int(11) NOT NULL,
  `secret` varchar(255) CHARACTER SET utf8 NOT NULL,
  `teamId` int(11) NOT NULL,
  `sprite` int(11),
  `color` varchar(11) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `playeranswerscore`
--
CREATE TABLE `playeranswerscore` (
`playerId` int(11)
,`teamId` int(11)
,`answerPoints` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Table structure for table `playerhint`
--

CREATE TABLE `playerhint` (
  `id` int(11) NOT NULL,
  `playerId` int(11) NOT NULL,
  `locationId` int(11) NOT NULL,
  `startTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `endTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Stand-in structure for view `playerlocationscore`
--
CREATE TABLE `playerlocationscore` (
`playerId` int(11)
,`locationPoints` decimal(23,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `playerpoints`
--
CREATE TABLE `playerpoints` (
`playerID` int(11)
,`teamID` int(11)
,`points` decimal(33,0)
,`elapsed` bigint(21)
);

-- --------------------------------------------------------

--
-- Table structure for table `playerquest`
--

CREATE TABLE `playerquest` (
  `id` int(11) NOT NULL,
  `playerId` int(11) NOT NULL,
  `questId` int(11) NOT NULL,
  `startTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `endTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `answer` varchar(11) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `PlayerRank`
--
CREATE TABLE `PlayerRank` (
`playerId` int(11)
,`points` decimal(33,0)
,`rank` bigint(22)
,`percentile` decimal(27,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `playerscore`
--
DROP VIEW IF EXISTS `playerscore`;
CREATE TABLE `playerscore` (
`playerId` int(11)
,`teamId` int(11)
,`answerPoints` decimal(32,0)
,`correct` decimal(23,0)
,`wrong` decimal(23,0)
,`maxTime` varchar(19)
,`minTime` varchar(19)
,`elapsed` bigint(21)
);

-- --------------------------------------------------------

--
-- Table structure for table `quest`
--

CREATE TABLE `quest` (
  `id` int(11) NOT NULL,
  `location` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `secret` varchar(255) CHARACTER SET utf8 NOT NULL,
  `question` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `color` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure for view `locationEvents`
--
DROP TABLE IF EXISTS `locationEvents`;

CREATE ALGORITHM=UNDEFINED DEFINER=`martelab`@`localhost` SQL SECURITY DEFINER VIEW `locationEvents`  AS  select `L`.`name` AS `name`,`PQ`.`startTime` AS `started`,`PQ`.`endTime` AS `ended`,timediff(`PQ`.`endTime`,`PQ`.`startTime`) AS `elapsed` from ((`playerquest` `PQ` join `quest` `Q` on((`Q`.`id` = `PQ`.`questId`))) join `location` `L` on((`L`.`id` = `Q`.`location`))) order by `PQ`.`startTime` ;

-- --------------------------------------------------------

--
-- Structure for view `locationstats`

--
DROP TABLE IF EXISTS `locationstats`;

CREATE ALGORITHM=UNDEFINED DEFINER=`martelab`@`localhost` SQL SECURITY DEFINER VIEW `locationstats`  AS  select `L`.`name` AS `name`,`L`.`type` AS `type`,(select count(0) from (`playerquest` `PQ` join `quest` `Q2` on((`Q2`.`id` = `PQ`.`questId`))) where (`Q2`.`location` = `L`.`id`)) AS `found`,(select count(0) from `playerhint` `PH` where (`PH`.`locationId` = `L`.`id`)) AS `suggested` from (`quest` `Q` join `location` `L` on((`Q`.`location` = `L`.`id`))) group by `L`.`id` order by `found` desc ;


-- --------------------------------------------------------

--
-- Structure for view `playerlocationscore`
--
DROP TABLE IF EXISTS `playerlocationscore`;

CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `playerlocationscore`  AS  select `p`.`id` AS `playerId`,sum((case when (`pq`.`id` is not null) then 1 else 0 end)) AS `locationPoints` from (`player` `p` left join `playerquest` `pq` on((`pq`.`playerId` = `p`.`id`))) group by `p`.`id` ;

-- --------------------------------------------------------

--
-- Structure for view `playerpoints`
--
DROP TABLE IF EXISTS `playerpoints`;

CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `playerpoints`  AS  select `pa`.`playerId` AS `playerID`,`pa`.`teamId` AS `teamID`,(`pa`.`answerPoints` + `pl`.`locationPoints`) AS `points`,
`pa`.`elapsed` AS `elapsed`
 from (`playerscore` `pa` join `playerlocationscore` `pl` on((`pa`.`playerId` = `pl`.`playerId`))) ;

-- --------------------------------------------------------

--
-- Structure for view `PlayerRank`
--
DROP TABLE IF EXISTS `PlayerRank`;

CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `PlayerRank`  AS  select `R1`.`playerId` AS `playerId`,`R1`.`points` AS `points`,`R1`.`rank` AS `rank`,round(((((`R2`.`t` - `R1`.`rank`) + 1) * 100) / `R2`.`t`),0) AS `percentile` from (((select `PC1`.`playerID` AS `playerId`,any_value(`PC1`.`points`) AS `points`,(count(`PC2`.`points`) + 1) AS `rank` from (`playerpoints` `PC1` left join `playerpoints` `PC2` on((`PC1`.`points` < `PC2`.`points`) OR (`PC1`.`points` = `PC2`.`points` AND `PC1`.`elapsed` > `PC2`.`elapsed`) )) group by `PC1`.`playerID`)) `R1` join (select count(0) AS `t` from `player`) `R2`) ;

-- --------------------------------------------------------

--
-- Structure for view `playerscore`
--
DROP TABLE IF EXISTS `playerscore`;

CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `playerscore`  AS  select `p`.`id` AS `playerId`,`p`.`teamId` AS `teamId`,sum((case when (`a`.`value` is not null) then `a`.`value` else 0 end)) AS `answerPoints`,sum((case when ((`a`.`value` > 0) and (`a`.`value` is not null)) then 1 else 0 end)) AS `correct`,sum((case when ((`a`.`value` = 0) and (`a`.`value` is not null)) then 1 else 0 end)) AS `wrong`,ifnull(max(`pq`.`endTime`),0) AS `maxTime`,ifnull(min(`pq`.`startTime`),0) AS `minTime`,ifnull(timestampdiff(SECOND,min(`pq`.`startTime`),max(`pq`.`endTime`)),0) AS `elapsed` from ((`player` `p` left join `playerquest` `pq` on((`pq`.`playerId` = `p`.`id`))) left join `answer` `a` on(((`a`.`quest` = `pq`.`questId`) and (convert(`a`.`name` using utf8) = `pq`.`answer`)))) group by `p`.`id` ;
--
-- Indexes for dumped tables
--

--
-- Indexes for table `answer`
--
ALTER TABLE `answer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quest` (`quest`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `player`
--
ALTER TABLE `player`
  ADD PRIMARY KEY (`id`),
  ADD KEY `teamId` (`teamId`),
  ADD KEY `teamId_2` (`teamId`);

--
-- Indexes for table `playerhint`
--
ALTER TABLE `playerhint`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questId` (`locationId`),
  ADD KEY `playerId` (`playerId`);

--
-- Indexes for table `playerquest`
--
ALTER TABLE `playerquest`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questId` (`questId`),
  ADD KEY `playerId` (`playerId`);

--
-- Indexes for table `quest`
--
ALTER TABLE `quest`
  ADD PRIMARY KEY (`id`),
  ADD KEY `location` (`location`) USING BTREE;

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answer`
--
ALTER TABLE `answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `player`
--
ALTER TABLE `player`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `playerhint`
--
ALTER TABLE `playerhint`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `playerquest`
--
ALTER TABLE `playerquest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `quest`
--
ALTER TABLE `quest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `answer`
--
ALTER TABLE `answer`
  ADD CONSTRAINT `answer_ibfk_1` FOREIGN KEY (`quest`) REFERENCES `quest` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `playerhint`
--
ALTER TABLE `playerhint`
  ADD CONSTRAINT `playerhint_ibfk_1` FOREIGN KEY (`locationId`) REFERENCES `location` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `playerhint_ibfk_2` FOREIGN KEY (`playerId`) REFERENCES `player` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `playerquest`
--
ALTER TABLE `playerquest`
  ADD CONSTRAINT `playerquest_ibfk_1` FOREIGN KEY (`playerId`) REFERENCES `player` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `playerquest_ibfk_2` FOREIGN KEY (`questId`) REFERENCES `quest` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `quest`
--
ALTER TABLE `quest`
  ADD CONSTRAINT `quest_ibfk_1` FOREIGN KEY (`location`) REFERENCES `location` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
