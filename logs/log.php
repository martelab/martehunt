<?php


if (! defined('INCLUDE_PATH')) {
	define('INCLUDE_PATH', dirname(__FILE__) . "");

    set_include_path(get_include_path() . PATH_SEPARATOR . dirname(__FILE__) . "../libs");
	set_include_path(get_include_path() . PATH_SEPARATOR . INCLUDE_PATH);
}

/**
 * Require the library
 */
require 'PhpTail.php';
/**
 * Initilize a new instance of PHPTail
 * @var PHPTail
 */
$tail = new PHPTail(array(
    "Grande Caccia" => "log.txt",
    "Apache Errors" => "/var/log/apache2/error.log",
));
/**
 * We're getting an AJAX call
 */
if(isset($_GET['ajax']))  {
    echo $tail->getNewLines($_GET['file'], $_GET['lastsize'], $_GET['grep'], $_GET['invert']);
    die();
}
/**
 * Regular GET/POST call, print out the GUI
 */
$tail->generateGUI();
?>