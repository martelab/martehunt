<?php

include 'global_init.php';

require_once 'services.php';
require_once 'log4php/LoggerManager.php';
require_once 'db/DaoManager.php';

$logger = & LoggerManager::getLogger(basename(__FILE__, '.php'));


///post data
$name = null;
$color = null;
$sprite = null;
if($_POST && isset($_POST["name"]))
{
 $name = $_POST["name"];
 $color = $_POST["color"];
 $sprite = $_POST["sprite"];
}


$daoManager = DaoManager::getInstance(DAO_CONFIG_PATH);

//1) get the user form the cookie
$playerDao = $daoManager->getDao("Player");
$playerStatDao = $daoManager->getDao("PlayerStat");

$player = $playerDao->fromToken("marteHunt");
if ($player == null) //stop here. No player registered
{
    //create a new user right now
    $player = createNewPlayer($playerDao, $logger);
}

if ($player->getName() != "unnamed")
{
    $logger->info("User " . $player->getName() . " ". $player->getId() ." tried to modify user again!");
    showLandingError();
    die();    
}

 $stat = $playerStatDao->selectByID($player->getId());
   
if ($name != null && $name != $player->getName())
{
    $playerDao->updateNameAndSprite($player, $name, $color, $sprite);
    $done = true;
    header("location: index.php");
}
else
{
  $done = false;
    
    $sprites = array();
  /*  for ($i = 1; $i <= 15; $i++) {
        $id = rand(9,59);
        $sy = -20 - 94*floor($id / 15);
        $sx = -14 - 36.5*floor($id % 15);

        $col = "#" . "30";
        $col = "#" .substr(bin2hex(rand()),0,6) . "30" ;
        array_push ($sprites, array("sx" =>$sx, "sy" =>$sy, "color" => $col ));
    }*/
}

$template = $twig->loadTemplate('register.html');
echo $template->render(array("stat" => $stat,
                                "player"=> $player,
                             "done"=>$done,
                             "sprites"=>$sprites
                                ));   
   
?>