![logo grande caccia.jpg](https://bitbucket.org/repo/9r8Mo7/images/2974463746-logo%20grande%20caccia.jpg)

### Introduction ###

A modern treasure hunt using smartphones by Marte Lab.

This is our code.

* version **1.0.0 Gorgo** (as used in Grogonzola during the 2-day event of 26 and 27 November 2016) 

### License ###

This software is a free software released under the 
[GPLv3 license](https://www.gnu.org/licenses/gpl-3.0.en.html)

### How it works ###

This application is delivered to single userst hrough a web server. 
Casual player will find printed hint in form of QR codes hidden in the ply area. 
By reading the code they are automatically registered in the game and firstly propted to select a team.
From that moment on they will score points by finding and solving each single quest.

Customization of the specific hunt can be done by inserting data in the relative DB and xml file.
Once ready 


### Dependencies ###

* [PureCSS](http://purecss.io)
* [Scrollorama](http://johnpolacek.github.io/scrollorama/)
* [JQuery](https://jquery.com)
* [Log4php](http://logging.apache.org/log4php/)
* [Twig](http://twig.sensiolabs.org/)
* [NemedianDao](https://bitbucket.org/nemedian/nemediandao) by Giampaolo Grieco and Pierluigi Taddei

### Installation ###

* The system can be automatically deployed using the uploader plugin found in /uploader. To do so set the various user/password in uploader/data_inc.php and activate a post-commit hook to the final server location of uploader/upload.php
* Once deployed set the db parameters under conf/db-conf.xml and setup adatabase using the structure provided in doc/marteHuntDB.sql
* Quests content can be set under domain/dao/quest.xml

### Known issues ###
None for now!


### Credits ###

A project by Marte Lab:

*  Brian Brivio <brian.brivio@gmail.com>
*  Simone Cocco <simone.cocco.76@gmail.com>
*  Paolo Insinga
*  Niccolò Leali <niccolo.leali@gmail.com>
*  Pierluigi Taddei <pierluigi.taddei@gmail.com>


### Contributing ###

If you would like to contribute please contact one of the authors