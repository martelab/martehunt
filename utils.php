<?php

/**
 * Generate a random string, using a cryptographically secure 
 * pseudorandom number generator (random_int)
 * 
 * For PHP 7, random_int is a PHP core function
 * For PHP 5.x, depends on https://github.com/paragonie/random_compat
 * 
 * @param int $length      How many characters do we want?
 * @param string $keyspace A string of all possible characters
 *                         to select from
 * @return string
 */
function random_str($length)
{
    $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $str = '';
    $max = mb_strlen($keyspace, '8bit') - 1;
    for ($i = 0; $i < $length; ++$i) {
        $str .= $keyspace[random_int(0, $max)];
    }
    return $str;
}

function haversine_distance($p1, $p2)
{
  $lat1 = $p1[0];
  $lon1 = $p1[1];
  
  $lat2 = $p2[0];
  $lon2 = $p2[1];
  
  $R = 6371e3; // metres
  $d1 = deg2rad($lat1);
  $d2 = deg2rad($lat2);
  $dphi = deg2rad($lat2-$lat1);
  $dlambda = deg2rad($lon2-$lon1);

  $a = sin($dphi/2) * sin($dphi/2) +
        cos($d1) * cos($d2) *
        sin($dlambda/2) * sin($dlambda/2);
  $c = 2 * atan2(sqrt($a), sqrt(1-$a));

  return $R * $c;
}

function wkt2point($s)
{
  preg_match('/POINT\((.+) (.+)\)/', $s, $matches, PREG_OFFSET_CAPTURE);
  if (count($matches) > 1)
  {
    return [floatval($matches[1][0]), floatval($matches[2][0])];
    
  }
}
function point2wkt($x)
{
  return "POINT(" . $x[0]. " " . $x[1] . "),4326"; #4326 for Italy SRID
}

 function xml2array($xml, $recurse)
    {
        $arr = array();

        foreach ($xml as $element)
        {
            $tag = $element->getName();
            $e = get_object_vars($element);
            if (!empty($e) )
            {
                if ($recurse &&  $element instanceof SimpleXMLElement)
                {
                    $arr[$tag] = xml2array($element, true) ;
                } else if ($recurse)
                {
                    $arr[$tag] = $e;
                }
            }
            else
            {
                $arr[$tag] = trim($element);
            }
        }

        return $arr;
    }

?>