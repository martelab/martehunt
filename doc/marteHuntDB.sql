-- phpMyAdmin SQL Dump
-- version 3.3.2deb1ubuntu1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: 12 ott, 2016 at 10:01 PM
-- Versione MySQL: 5.1.69
-- Versione PHP: 5.3.2-1ubuntu4.30.1~gandi

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `martelab`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `Player`
--

CREATE TABLE IF NOT EXISTS `Player` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(11) CHARACTER SET utf8 NOT NULL,
  `active` int(11) NOT NULL,
  `secret` varchar(255) CHARACTER SET utf8 NOT NULL,
  `teamId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `teamId` (`teamId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dump dei dati per la tabella `Player`
--


-- --------------------------------------------------------

--
-- Struttura della tabella `PlayerQuest`
--

CREATE TABLE IF NOT EXISTS `PlayerQuest` (
  `id` int(11) NOT NULL,
  `playerId` int(11) NOT NULL,
  `questId` int(11) NOT NULL,
  `startTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `endTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `answer` varchar(11) CHARACTER SET utf8 NOT NULL,
  `solved` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `playerId` (`playerId`,`questId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `PlayerQuest`
--


-- --------------------------------------------------------

--
-- Struttura della tabella `Quest`
--

CREATE TABLE IF NOT EXISTS `Quest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `secret` varchar(255) CHARACTER SET utf8 NOT NULL,
  `pageName` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dump dei dati per la tabella `Quest`
--


-- --------------------------------------------------------

--
-- Struttura della tabella `Team`
--

CREATE TABLE IF NOT EXISTS `Team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `color` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dump dei dati per la tabella `Team`
--

INSERT INTO `Team` (`id`, `name`, `color`) VALUES
(1, 'Argentia', 'D1F2A5'),
(2, 'Molgoretta', 'EFFAB4');
