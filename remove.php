<?php

include 'global_init.php';

require_once 'services.php';
require_once 'log4php/LoggerManager.php';
require_once 'db/DaoManager.php';


$logger = & LoggerManager::getLogger(basename(__FILE__, '.php'));


$daoManager = DaoManager::getInstance(DAO_CONFIG_PATH);

//1) get the user form the cookie
$playerDao = $daoManager->getDao("Player");
$playerQuestDao = $daoManager->getDao("PlayerQuest");
$playerHintDao = $daoManager->getDao("PlayerHint");

$player = $playerDao->fromToken("marteHunt");

if ($player == null) //stop here. No player registered
{
    showLandingError();
    die();
}

$cleared = $playerDao->clearToken("marteHunt");


if (!$cleared)
{
    $logger->info("player " . $player->getId() . " tried to remove account");
    showRemoval(false);
    die();
}

$daoManager->startTransaction();
$done = $playerDao->removePlayer($player);

if ($done)
{
    $playerHintDao->deletePlayer($playerId);
    $playerQuestDao->deletePlayer($playerId);
}
$daoManager->commitTransaction();

$logger->info("player " . $player->getId() . " removed account");
showRemoval(true);
die();
?>