<?php
include 'global_init.php';

require_once 'utils.php';
require_once 'services.php';
require_once 'log4php/LoggerManager.php';
require_once 'db/DaoManager.php';


$logger = & LoggerManager::getLogger(basename(__FILE__, '.php'));

$daoManager = DaoManager::getInstance(DAO_CONFIG_PATH);

//1) get the user form the cookie
$playerDao = $daoManager->getDao("Player");

$player = $playerDao->fromToken("marteHunt");

if ($player == null) //let's register a new user
{
    //create a new user right now
    $player = createNewPlayer($playerDao, $logger);
}
else
{
 $logger->info( $player->getId()." player. Welcome back to info");
}

//2) get the player un-resolved quests and the requested quest
$playerQuestDao = $daoManager->getDao('PlayerQuest');

$statDao = $daoManager->getDao('PlayerStat');
$stat = $statDao->selectById($player->getId());

$locationDao = $daoManager->getDao('Location');


//get the unsolved list of location quest
$locations = $locationDao->selectUnsolved($player->getId());

$options = array();
if (count($locations) == 0)
{
    $options["completed"] = true;
}

showPlayerStatus($locationDao, $player, $stat, $options, $twig);

die();

?>
