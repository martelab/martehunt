<?php

require_once 'domain/Quest.php';
require_once 'log4php/LoggerManager.php';
require_once 'db/interfaces.php';
require_once 'db/Dao.php';
require_once 'utils.php';

class QuestDao extends Dao {
	
	private static $CLASS = "domain/Quest";
	private static $DAO_SQL_PATH = INCLUDE_PATH . "/domain/dao/QuestDao.xml";
	
	//SQL STATEMENT query keys
    private static $INSERT = "insert";
    private static $INSERT_ANSWER = "insert_answer";
    private static $SELECT_ANSWERS = "select_answers";
    private static $SELECT_BY_LOCATION_UNRESOLVED = "select_by_location_unresolved";
    private static $SELECT_BY_LOCATION_NEW_RANDOM = "select_by_location_new_random";
    private static $SELECT_BY_LOCATION_NAME = "select_by_location_name";
    private static $SELECT_BY_LOCATION_NAME_AND_PLAYER = "select_by_location_name_and_player";
    private static $SELECT_ALL = "select_all";
    
    private static $SELECT_BY_ID = "select_by_id";

	private static $logger;
	
	public function __construct(QueryRunner $queryRunner) {
		parent::__construct($queryRunner);
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
		$this->addStatementsPath(self::$DAO_SQL_PATH);
	}
	
    
    public function create($xml) 
    {    
        $d = $this->map2object(xml2array($xml,false), self::$CLASS);
        $d->setQuestion((string)$d->getQuestion());
        $d->setSecret(random_str(8));
        return $d;
    }
    public function insert($p) 
    {
        $args = array($p->getLocation(), $p->getName(), $p->getSecret(), $p->getQuestion());
        $id = $this->execute($this->getStatement(self::$INSERT), $args);
        if ($id < 0) return null;
        
        $p->setId($id);

        $ansArg = array();
        foreach ($p->getAnswers() as $a)
        {
           array_push($ansArg, array($id, $a['name'], $a['text'], $a['value']));
        }

        //now insert answers
        $this->execute($this->getStatement(self::$INSERT_ANSWER), $ansArg);        
    }
    
    public function selectByLocationName($locationName) {
		$args = array($locationName);
		return $this->queryForList($this->getStatement(self::$SELECT_BY_LOCATION_NAME), $args, self::$CLASS);
	}
      public function selectByLocationName_and_player($locationName, $playerId) {
		$args = array($locationName, $playerId);
		return $this->queryForList($this->getStatement(self::$SELECT_BY_LOCATION_NAME_AND_PLAYER), $args, self::$CLASS);
	}
    
    public function selectAll() {
        $args = array();
		$quests = $this->queryForList($this->getStatement(self::$SELECT_ALL), $args, self::$CLASS);
    
        foreach($quests as $q)
        {
          //now load answers
          $ans = $this->queryForList($this->getStatement(self::$SELECT_ANSWERS), array($q->getId() ));
          $q->setAnswers($ans);
        }
      return $quests;
	}
    
    public function selectByLocation_new($locationId, $secret, $playerId) {
        //check if there is a new quest to display for this user    
        $args = array($locationId, "%".$secret."%", $playerId);
        $q = $this->queryForObject($this->getStatement(self::$SELECT_BY_LOCATION_NEW_RANDOM), $args, self::$CLASS);
        if ($q == null) return null;
        
        //now load answers
        $ans = $this->queryForList($this->getStatement(self::$SELECT_ANSWERS), array($q->getId() ));
        $q->setAnswers($ans);
        return $q;
    }
    public function selectByLocation_unresolved($locationId, $secret, $playerId) {
        //check if there is an unresolved entry
        $args = array($locationId, "%".$secret."%", $playerId);
		$q = $this->queryForObject($this->getStatement(self::$SELECT_BY_LOCATION_UNRESOLVED), $args, self::$CLASS);
        if ($q == null) return null;
        
        //now load answers
        $ans = $this->queryForList($this->getStatement(self::$SELECT_ANSWERS), array($q->getId() ));
        $q->setAnswers($ans);
        return $q;
	}

}
?>