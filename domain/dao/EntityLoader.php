<?php

require_once 'log4php/LoggerManager.php';
require_once 'db/Proxy.php';

abstract class EntityLoader extends Proxy {

	private static $logger;

	public function __construct($entity){
		parent::__construct($entity);
		if (self::$logger == null){
			self::$logger =& LoggerManager::getLogger(get_class($this));
		}
	}

	public function getEntity(){
		return $this->o;
	}

	protected function begin($name, $arguments){
		return;
	}

	protected function end($name, $arguments, $result){
		return;
	}

}
?>