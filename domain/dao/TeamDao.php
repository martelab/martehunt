<?php

require_once 'domain/Team.php';
require_once 'log4php/LoggerManager.php';
require_once 'db/interfaces.php';
require_once 'db/Dao.php';

class TeamDao extends Dao {
	
	private static $CLASS = "domain/Team";
	private static $DAO_SQL_PATH = INCLUDE_PATH . "/domain/dao/TeamDao.xml";
	
	//SQL STATEMENT query keys
    private static $DELETE_ALL = "remove_all";
    private static $RESET = "reset";
    private static $INSERT = "insert";
	private static $SELECT_BY_PK = "select_by_pk";
	private static $SELECT_BY_NAME = "select_by_name";
	private static $SELECT_ALL = "select_all";
	private static $SELECT_ALL_SCORE = "select_with_points";
    
	private static $logger;
	
	public function __construct(QueryRunner $queryRunner) {
		parent::__construct($queryRunner);
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
		$this->addStatementsPath(self::$DAO_SQL_PATH);
	}
	public function removeAll() { 
     $this->execute($this->getStatement(self::$DELETE_ALL));
     $this->execute($this->getStatement(self::$RESET));
    } 
    
    public function create($xml) {    
        $d = $this->map2object(xml2array($xml,false), self::$CLASS);
        return $d;
    }
      
    public function insert(Team $p) {
		$args = array($p->getName(), $p->getColor());
        $id = $this->execute($this->getStatement(self::$INSERT), $args);
		if ($id >= 0)
        {
            $p->setId($id);
            return $p;
        }
        else return null;
	}
    
    
	public function selectByPK($userId) {
		$args = array($$userId);
		$loader = $this->queryForObject($this->getStatement(self::$SELECT_BY_PK), $args, self::$CLASS);
		if ($loader != null) {
			return $loader; //->getEntity();
		} else
			return null;
	}
	
	public function selectByName($userName) {
		$args = array($userName);
		$loader = $this->queryForObject($this->getStatement(self::$SELECT_BY_NAME), $args, self::$CLASS);
		if ($loader != null) {
			return $loader; //->getEntity();
		} else
			return null;
	}
    
    public function selectAll() {
        $args = array();
        $toReturn = array();
		$loaders = $this->queryForList($this->getStatement(self::$SELECT_ALL), $args, self::$CLASS);
		foreach ($loaders as $loader) {
			//$entity = $loader->getEntity();
			array_push($toReturn, $loader);
		}
		return $toReturn;
	}
    
    public function selectAll_score() {
        $args = array();
        $toReturn = array();
		$loaders = $this->queryForList($this->getStatement(self::$SELECT_ALL_SCORE), $args, self::$CLASS);
		foreach ($loaders as $loader) {
			//$entity = $loader->getEntity();
			array_push($toReturn, $loader);
		}
		return $toReturn;
	}
}
?>