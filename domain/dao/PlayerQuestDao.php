<?php
require_once 'domain/PlayerQuest.php';
require_once 'log4php/LoggerManager.php';
require_once 'db/interfaces.php';
require_once 'db/Dao.php';

class PlayerQuestDao extends Dao {
	
	private static $CLASS = "domain/PlayerQuest";
	private static $DAO_SQL_PATH = INCLUDE_PATH . "/domain/dao/PlayerQuestDao.xml";
	
	//SQL STATEMENT query keys
	private static $SELECT_BY_QUESTID_PLAYERID = "select_by_questID_playerID";
	private static $SELECT_ALL = "select_all";
    private static $CHECK_CORRECT = "checkCorrect";
    private static $INSERT = "insert";
    private static $ANSWER = "answer";
  	private static $DELETE_PLAYER = "delete_player";
    
	private static $logger;
	
	public function __construct(QueryRunner $queryRunner) {
		parent::__construct($queryRunner);
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
		$this->addStatementsPath(self::$DAO_SQL_PATH);
	}

    public function insert($playerId, $questId) {
		$args = array($playerId, $questId);
		return $this->execute($this->getStatement(self::$INSERT), $args, self::$CLASS);
	}
	public function setAnswer($playerId, $questId, $a) {
		$args = array($playerId, $questId, $a);
		return $this->execute($this->getStatement(self::$ANSWER), $args, self::$CLASS);
	}
    
    public function removePlayer($playerId) {
		$args = array($playerId);
		return $this->execute($this->getStatement(self::$DELETE_PLAYER), $args);
	}
    
	public function selectByQuestIDPlayerID() {
		$args = array();
		$loader = $this->queryForObject($this->getStatement(self::$SELECT_BY_QUESTID_PLAYERID), $args, self::$CLASS);
		if ($loader != null) {
			return $loader;
		} else
			return null;
	}
    
    public function checkCorrectness($playerID, $questId) {
		$args = array($playerID, $questId);
		$x = $this->queryForObject($this->getStatement(self::$CHECK_CORRECT), $args);
        return $x["correct"] == 1;
	}

    public function selectAll() {
        $args = array();
        $toReturn = array();
		$loaders = $this->queryForList($this->getStatement(self::$SELECT_ALL), $args, self::$CLASS);
		foreach ($loaders as $loader) {
			$entity = $loader;
			array_push($toReturn, $entity);
		}
		return $toReturn;
	}
    
}
?>