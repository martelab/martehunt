<?php

require_once 'domain/Location.php';
require_once 'log4php/LoggerManager.php';
require_once 'db/interfaces.php';
require_once 'db/Dao.php';
require_once 'utils.php';

class LocationDao extends Dao {

	  private static $CLASS = "domain/Location";
    private static $DAO_SQL_PATH = INCLUDE_PATH . "/domain/dao/LocationDao.xml";

    //query keys
    private static $SELECT_ALL = "select_all";
	  private static $SELECT_BY_ID = "select_by_id";
    private static $SELECT_BY_NAME = "select_by_name";
    private static $INSERT = "insert_location";
		private static $INSERT_WITH_ID = "insert_location_id";
    private static $DELETE_ALL = "remove_all";
    private static $RESET = "reset";
    private static $COUNT = "count";

    private static $SELECT_UNSOLVED = "select_unsolved";
    private static $SELECT_SOLVED = "select_solved";
    private static $SELECT_HINTED = "select_hinted";
    private static $SELECT_NEW_HINT= "select_newhint";

	private static $logger;

	public function __construct(QueryRunner $queryRunner) {
		parent::__construct($queryRunner);
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
        $this->addStatementsPath(self::$DAO_SQL_PATH);
    }
    public function removeAll() {
     $this->execute($this->getStatement(self::$DELETE_ALL));
     $this->execute($this->getStatement(self::$RESET));
    }

    public function countLocations() {
       $c = $this->queryForObject($this->getStatement(self::$COUNT));
      return $c["count"];
    }

    public function create($xml) {
        $d = $this->map2object(xml2array($xml,false), self::$CLASS);
        $d->setName((string)$xml["name"]);
        $d->setType((string)$xml["type"]);

				//set the secret if present
        $secret = (string)$xml["secret"];
        if ($secret == null)  $secret = random_str(16);
        $d->setSecret( $secret );

				//set the id if present
				$id = (int)$xml["id"];
				if ($id != null)  $d->setId($id);

        return $d;
    }

    public function insert(Location $p) {

			if ($p->getId() != null)
			{
				$args = array($p->getId(), $p->getName(), $p->getSecret(), $p->getHint(), $p->getType());
				$id = $this->execute($this->getStatement(self::$INSERT_WITH_ID), $args);
			} else {
					$args = array($p->getName(), $p->getSecret(), $p->getHint(), $p->getType());
	        $id = $this->execute($this->getStatement(self::$INSERT), $args);
			}

			if ($id >= 0)
      {
          $p->setId($id);
          return $p;
      }
      else return null;
	}
    public function selectAll() {
		return  $this->queryForList($this->getStatement(self::$SELECT_ALL), [], self::$CLASS);
	}
     public function selectUnsolved($playerId) {
        $args = array($playerId);
		return  $this->queryForList($this->getStatement(self::$SELECT_UNSOLVED), $args, self::$CLASS);
	}
    public function selectSolved($playerId) {
        $args = array($playerId);
		return  $this->queryForList($this->getStatement(self::$SELECT_SOLVED), $args, self::$CLASS);
	}
    public function selectHinted($playerId) {
        $args = array($playerId);
		return  $this->queryForList($this->getStatement(self::$SELECT_HINTED), $args, self::$CLASS);
	}
    public function selectNewHint($playerId) {
        $args = array($playerId,$playerId);
		return  $this->queryForObject($this->getStatement(self::$SELECT_NEW_HINT), $args, self::$CLASS);
	}
    public function selectByName($name) {
        $args = array($name);
		return  $this->queryForObject($this->getStatement(self::$SELECT_BY_NAME), $args, self::$CLASS);
	}
}
?>
