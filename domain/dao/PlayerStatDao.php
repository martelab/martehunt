<?php

require_once 'domain/PlayerStat.php';
require_once 'log4php/LoggerManager.php';
require_once 'db/interfaces.php';
require_once 'db/Dao.php';

class PlayerStatDao extends Dao {
	
	private static $CLASS = "domain/PlayerStat";
	private static $DAO_SQL_PATH = INCLUDE_PATH . "/domain/dao/PlayerStatDao.xml";
	
	//SQL STATEMENT query keys
	private static $SELECT_BY_PLAYER = "select_by_player";
    private static $SELECT_REGISTERED_RANK = "select_registered_rank";
	private static $logger;
	
	public function __construct(QueryRunner $queryRunner) {
		parent::__construct($queryRunner);
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
		$this->addStatementsPath(self::$DAO_SQL_PATH);
	}
	
	public function selectByID($userId) {
		$args = array($userId);
		return $this->queryForObject($this->getStatement(self::$SELECT_BY_PLAYER), $args, self::$CLASS);
	}
    
    public function selectRegistered_score() {
		$args = array();
		return $this->queryForList($this->getStatement(self::$SELECT_REGISTERED_RANK), $args, self::$CLASS);
	}

}
?>