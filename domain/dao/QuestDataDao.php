<?php

require_once 'domain/QuestData.php';
require_once 'log4php/LoggerManager.php';
require_once 'db/interfaces.php';
require_once 'db/Dao.php';

class QuestDataDao extends Dao {
	
	private static $CLASS = "domain/QuestData";
	private static $DAO_XML_PATH = INCLUDE_PATH . "/domain/dao/quests.xml";
	
	//query keys
	private static $SELECT_BY_ID = "select_by_id";
	private static $logger;
    
    private static $XML;
	
	public function __construct(QueryRunner $queryRunner) {
		parent::__construct($queryRunner);
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
        if (!self::$XML)
            self::$XML = simplexml_load_file(self::$DAO_XML_PATH);
	}
	
    public function countQuests() {
        $list = self::$XML->xpath('//quest');
        return count($list);
    }
    
    private function xml2array($xml, $recurse)
    {
        $arr = array();

        foreach ($xml as $element)
        {
            $tag = $element->getName();
            $e = get_object_vars($element);
            if (!empty($e) )
            {
                if ($recurse &&  $element instanceof SimpleXMLElement)
                {
                    $arr[$tag] = $this->xml2array($element, true) ;
                } else if ($recurse)
                {
                    $arr[$tag] = $e;
                }
            }
            else
            {
                $arr[$tag] = trim($element);
            }
        }

        return $arr;
    }
    
    
	public function selectByID($questId) {

        if (!self::$XML) throw new DaoException("Error loading QuestData");

        $q = self::$XML->xpath('//quest[@id="'. $questId .'"]');
   
        if (count($q) != 1)  throw new DaoException("No quest found for given id " . $questId);

		
        $d = $this->map2object($this->xml2array($q[0],false), self::$CLASS);
        
        $ans = $q[0]->xpath('answer');
        $d->setId($questId);
        $answerData = array();
        foreach($ans as $a)
        {
            array_push($answerData, array("text" => (string) $a, "letter" => (string) $a['letter'], "correct" => (bool) $a["v"] == "true"));
        }
        $d->setAnswers($answerData);
        return $d;
	}
}
?>