<?php

require_once 'domain/Player.php';
require_once 'log4php/LoggerManager.php';
require_once 'db/interfaces.php';
require_once 'db/Dao.php';
require_once 'utils.php';

class PlayerDao extends Dao {
	
	private static $CLASS = "domain/Player";
	private static $DAO_SQL_PATH = INCLUDE_PATH . "/domain/dao/PlayerDao.xml";
	
	//SQL STATEMENT query keys
	private static $SELECT_BY_PK = "select_by_pk";
  	private static $SELECT_BY_ID_UNSAFE = "select_by_id_unsafe";
	private static $SELECT_BY_NAME = "select_by_name";
	private static $INSERT_PLAYER = "insert_player";
	private static $SELECT_BY_TEAM = "select_by_team";
	private static $INSERT_TEAM = "insert_team";
    private static $CHANGE_NAME = "change_name";
    private static $CHANGE_NAME_AND_SPRITE = "change_name_and_sprite";
    private static $REMOVE_BY_ID = "remove_by_id";
    private static $UPDATE_POSITION = "update_position";	
    private static $SELECT_BY_LAST_LOCATION = "select_last_position";
	private static $logger;
	
	public function __construct(QueryRunner $queryRunner) {
		parent::__construct($queryRunner);
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
		$this->addStatementsPath(self::$DAO_SQL_PATH);
	}
	
  public function select_by_id_unsafe($userId) {
      $args = array($userId,);
		$loader = $this->queryForObject($this->getStatement(self::$SELECT_BY_ID_UNSAFE), $args, self::$CLASS);
		if ($loader != null) {
			return $loader; //->getEntity();
		} else
			return null;
	}
  
  public function selectByPK($userId, $secret) {
		$args = array($userId, $secret);
		$loader = $this->queryForObject($this->getStatement(self::$SELECT_BY_PK), $args, self::$CLASS);
		if ($loader != null) {
			return $loader; //->getEntity();
		} else
			return null;
	}
	
	public function selectByName($userName) {
		$args = array($userName);
		$loader = $this->queryForObject($this->getStatement(self::$SELECT_BY_NAME), $args, self::$CLASS);
		if ($loader != null) {
			return $loader; //->getEntity();
		} else
			return null;
	}
	
	public function selectByLogin(Login $login) {
		$args = array($login->getAuthKey(), $login->getSession(), $login->getIp());
		$loader = $this->queryForObject($this->getStatement(self::$SELECT_BY_LOGIN), $args, self::$CLASS);
		if ($loader != null) {
			return $loader; //->getEntity();
		} else
			return null;
	}
    
    public function selectByTeam($teamId) {
        $args = array($teamId);
		$loaders = $this->queryForList($this->getStatement(self::$SELECT_BY_TEAM), $args, self::$CLASS);
		foreach ($loaders as $loader) {
			$entity = $loader; //->getEntity();
			array_push($toReturn, $entity);
		}
		return $toReturn;
	}
	
	public function updatePlayerPosition(Player $p) {
		$args = array($p->getId(), point2wkt([ $p->getLat(), $p->getLon()]));
		return $this->execute($this->getStatement(self::$UPDATE_POSITION), $args);
	}
    public function getLastPosition($playerId) {
		$args = array($playerId);
		$res = $this->queryForObject($this->getStatement(self::$LAST_POSITION), $args);
        return wkt2point($res["pos"]);
	}
    public function selectByLastPosition() {
        $toReturn = array();
		$loaders = $this->queryForList($this->getStatement(self::$SELECT_BY_LAST_LOCATION), [], self::$CLASS);
		foreach ($loaders as $loader) {
			$loader->setPosition(wkt2point($loader->getPosition())); 
			array_push($toReturn, $loader);
		}
		return $toReturn;
	}
  
  
	public function insertPlayer(Player $p) {
		$args = array($p->getName(), $p->getSecret());
		$p->setId($this->execute($this->getStatement(self::$INSERT_PLAYER), $args));
        return $p;
	}
    
    public function removePlayer(Player $p) {
		$args = array($p->getId(), $p->getSecret());
		return $this->execute($this->getStatement(self::$REMOVE_BY_ID), $args);
	}
    
    public function clearToken($tokenName) {
        //make sure there is not a clearLock
        
        $lockkey = $tokenName."_lo";
        if (isset($_COOKIE[$lockkey])) return false;
            
        $idkey = $tokenName . "_id";
        $seckey = $tokenName . "_sec";
        
        setcookie($idkey, null, time());
        setcookie($seckey, null, time());
            
        //lock delete for 30 minutes    
        setcookie($lockkey, "lo", time() + 1800);
        return true;
    }

    public function fromToken($tokenName) {
        $idkey = $tokenName . "_id";
        $seckey = $tokenName . "_sec";

        $latkey = $tokenName . "_lat";
        $lonkey = $tokenName . "_lon";

        
        
		if (!isset($_COOKIE[$idkey]) || !isset($_COOKIE[$seckey]) ) return null;
        else 
        {
            $id = $_COOKIE[$idkey];
            $secret = $_COOKIE[$seckey];
 
			$p = $this->selectByPK($id, $secret);
            
            if ($p != null && isset($_COOKIE[$latkey]))
            {
              $p->setPosition([floatval($_COOKIE[$latkey]), floatval($_COOKIE[$lonkey])]);      
            }
			return $p;
        }
    }
    
    public function toToken(Player $p, $tokenName) {
        $idkey = $tokenName . "_id";
        $seckey = $tokenName . "_sec";
        if (isset($_COOKIE[$idkey]) && isset($_COOKIE[$seckey]) ) 
        {
            self::$logger->info("cannot set cookie for player " . $p->getId());
            return false;
        } 
        else 
        {
            $done1 = setcookie($idkey, $p->getId(), time() + 2*86400);
            $done2 = setcookie($seckey, $p->getSecret(), time() + 2*86400);
			
            self::$logger->info("setting cookie for player " . $p->getId());
            if (!$done1 || $done2) 	self::$logger->error("cannot set cookie for player " . $p->getId());
            
            return $done1 && $done2;
        }
    }
	
    public function insertTeam(Player $p, $teamId) {
			$args = array($teamId, $p->getId());
			$r = $this->execute($this->getStatement(self::$INSERT_TEAM), $args, self::$CLASS);
            $p->setTeamId($teamId);
            return $p;
	}
	    
    public function updateName(Player $p, $newName) {
			$args = array($newName, $p->getId());
			return $this->execute($this->getStatement(self::$CHANGE_NAME), $args, self::$CLASS);
	}
    	    
    public function updateNameAndSprite(Player $p, $newName, $color, $sprite) {
			$args = array($newName, $color, $sprite, $p->getId());
			return $this->execute($this->getStatement(self::$CHANGE_NAME_AND_SPRITE), $args, self::$CLASS);
	}
}
?>