<?php

require_once 'domain/Location.php';
require_once 'log4php/LoggerManager.php';
require_once 'db/interfaces.php';
require_once 'db/Dao.php';
require_once 'utils.php';

class QuestConstraintsDao extends Dao {
	
	private static $CLASS = "domain/QuestConstraints";
    private static $DAO_SQL_PATH = INCLUDE_PATH . "/domain/dao/QuestConstraintsDao.xml";
	
    private static $SETTINGS_PATH = INCLUDE_PATH . "/conf/config.xml";
  
    //settings
    private static $SETTINGS = null;
  
    //query keys
    private static $SELECT_BY_QUEST = "select_by_quest";
	private static $SELECT_FULL = "select_full";
    private static $INSERT = "insert";
    private static $DELETE_ALL = "remove_all";
    private static $COMPLETE = "complete";
	private static $logger;
    
	public function __construct(QueryRunner $queryRunner) {
		parent::__construct($queryRunner);
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
        $this->addStatementsPath(self::$DAO_SQL_PATH);
      
      
        if (self::$SETTINGS == null)
        {
          //TODO create a settings object instead?
          $xml = simplexml_load_file(self::$SETTINGS_PATH);

          $s = $xml->xpath('constraints')[0];
          self::$SETTINGS = xml2array($s, true);

          self::$SETTINGS["rank"]["enabled"] = (self::$SETTINGS["rank"]["enabled"] == "true" );
          self::$SETTINGS["rank"]["delay"] = (float)self::$SETTINGS["rank"]["delay"];
          self::$SETTINGS["rank"]["max"] = (int)self::$SETTINGS["rank"]["max"];
          
          self::$SETTINGS["position"]["enabled"] = (self::$SETTINGS["position"]["enabled"] == "true" );
          
           self::$SETTINGS["manual"]["enabled"] = (self::$SETTINGS["manual"]["enabled"] == "true" );
        }
    }
    public function removeAll() { 
     $this->execute($this->getStatement(self::$DELETE_ALL));
    } 
     
    public function create($xml) {    

        $d = $this->map2object(xml2array($xml,false), self::$CLASS);
        $d->setSettings(self::$SETTINGS);

        return $d;
    }
    
    public function insert(QuestConstraints $p) {
		$args = array($p->getQuest(), $p->isPosConstraint(), point2wkt($p->getLatLon()), $p->getMaxDistance(), $p->isManualConstraint());

        $id = $this->execute($this->getStatement(self::$INSERT), $args);
		if ($id >= 0)
        {
            return $p;
        }
        else return new QuestConstraints();
	}
  
    public function setCompleted($constraintId,$playerID) {
        $args = array($constraintId,$playerID);
        $id = $this->execute($this->getStatement(self::$COMPLETE), $args);
		if ($id >= 0) return true;
        else return false;
    }
    public function select($questId, $playerId) {
        $args = array($playerId,$playerId,$questId);
		$v =  $this->queryForObject($this->getStatement(self::$SELECT_FULL), $args, self::$CLASS);
        if ($v != null)
		{
          $v->setSettings(self::$SETTINGS);
          if ($v->isPosConstraint())  $v->setLatLon(wkt2point($v->getLatLon()));
		  return $v;
        } else {
          $v = $this->map2object([], self::$CLASS);
          $v->setSettings(self::$SETTINGS);
         return $v;
	   }
    }
  
    public function select_by_quest($questId) {
      $args = array($questId);
      return $this->queryForList($this->getStatement(self::$SELECT_BY_QUEST), $args, self::$CLASS);
      
    }
  }
?>