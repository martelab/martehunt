<?php

require_once 'domain/PlayerQuest.php';
require_once 'log4php/LoggerManager.php';
require_once 'db/interfaces.php';
require_once 'db/Dao.php';

class PlayerHintDao extends Dao {
	
	private static $CLASS = "domain/PlayerHint";
	private static $DAO_SQL_PATH = INCLUDE_PATH . "/domain/dao/PlayerHintDao.xml";
	
	//SQL STATEMENT query keys
	private static $INSERT = "insert_hint";
	private static $DELETE_PLAYER = "delete_player";
	
	private static $logger;
	
	public function __construct(QueryRunner $queryRunner) {
		parent::__construct($queryRunner);
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
		$this->addStatementsPath(self::$DAO_SQL_PATH);
	}
    public function removePlayer($playerId) {
		$args = array($playerId);
		return $this->execute($this->getStatement(self::$DELETE_PLAYER), $args);
	}
    
	public function insert($playerId, $locationId) {
		$args = array($playerId, $locationId);
		return $this->execute($this->getStatement(self::$INSERT), $args, self::$CLASS);
	}

}
?>