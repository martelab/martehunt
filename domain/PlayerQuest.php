<?php

require_once 'log4php/LoggerManager.php';

class PlayerQuest
{
	private static $logger;
	
	private $id;
	
	private $playerId;
	
	private $questId;
	
	private $startTime;
    
    private $endTime;
    
    private $answer;
	
    private $solved;
	
	public function __construct() {
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
	}
	
	public function setId($id) {
		$this->id = $id;
	}
	
	public function getId() {
		return $this->id;
	}
	
	public function setPlayerId($playerId) {
		$this->playerId = $playerId;
	}
	
	public function getPlayerId() {
		return $this->playerId;
	}
	
	public function setQuestId($questId) {
		$this->questId = $questId;
	}
	
	public function getQuestId() {
		return $this->questId;
	}
    
    public function setStartTime($startTime) {
        $this->startTime = $startTime;
    }
    
    public function getStartTime() {
        return $this->startTime;
    }
    
    public function setEndTime($endTime) {
		$this->endTime = $endTime;
	}
	
	public function getEndTime() {
		return $this->endTime;
	}
    
    public function setAnswer($answer) {
		$this->answer = $answer;
	}
	
	public function getAnswer() {
		return $this->answer;
	}
	
	public function setSolved($solved) {
		$this->solved = $solved;
	}
	
	public function getSolved() {
		return $this->solved;
	}
}

?>