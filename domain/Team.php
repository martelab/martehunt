<?php

require_once 'log4php/LoggerManager.php';

class Team
{
	private static $logger;
	
	private $id;

	private $active;
	
	public $name;
	    
    public $score;
    
    public $color;
	
	public function __construct() {
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
	}
	
	public function setId($id) {
		$this->id = $id;
	}
	
	public function getId() {
		return $this->id;
	}
    public function setScore($s) {
		$this->score = ($s == "")? 0 : $s;
	}
	
	public function getScore() {
		return $this->score;
	}
	
	public function setName($name) {
		$this->name = $name;
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function setColor($s) {
		$this->color = $s;
	}
	
	public function getColor() {
		return $this->color;
	}
}

?>