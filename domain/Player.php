<?php

require_once 'log4php/LoggerManager.php';

class Player {
	
	private static $logger;
	
	private $id;
	
	private $name;
	
	private $secret;
	
	private $active;
	
	private $teamId;
    
    private $position;
	
	public function __construct() {
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
	}
	
	public function setId($id) {
		$this->id = $id;
	}
	
	public function getId() {
		return $this->id;
	}
	
	public function setName($name) {
		$this->name = $name;
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function setSecret($s) {
		$this->secret = $s;
	}
	
	public function getSecret() {
		return $this->secret;
	}
	
    public function isActive() {
        return $active;
    }
    
    public function setActive($a) {
        $active = $a;
    }
	
    public function setTeamId($teamId) {
        $this->teamId = $teamId;
    }
    
	public function getTeamId() {
		return $this->teamId;
	}
    
    public function setPosition($x) {
		$this->position = $x;
	}
    public function getPosition() {
		return $this->position;
	}
     public function getLat() {
		return $this->position[0];
	}
     public function getLon() {
		return $this->position[1];
	}

}

?>