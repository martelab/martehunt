<?php


class PlayerStat {

	private $playerId;
    
    public $playerName;
    
    public $teamName;
    
	private $correct;
	
	public $points;
    
    private $wrong;
	
	private $time;
	
	private $percentile;
    
	private $rank;
    
    public $color;
    
    public $sprite;
	
	public function __construct() {
	}
	
	public function setPlayerId($id) {
		$this->playerId = $id;
        $this->sprite = $this->getSprite();
	}
	
	public function getPlayerId() {
		return $this->playerId;
	}
	
    public function getName() {
		return $this->playerName;
	}
    public function setName($x) {
		$this->playerName = $x;
        $this->color = $this->getColor();
	}
    public function getTeamName() {
		return $this->teamName;
	}
    public function setTeamName($x) {
		$this->teamName = $x;
	}
    	
	public function setCorrect($x) {
		$this->correct = $x;
	}
    	
	public function setWrong($x) {
		$this->wrong = $x;
	}
	public function setPoints($x) {
		$this->points = $x;
	}
	
	public function setTime($x) {
		$this->time = $x;
	}
	
	public function setPercentile($x) {
		$this->percentile = $x;
	}
    
	public function setRank($x) {
		$this->rank = $x;
	}
    
	public function getCorrect() {
		return $this->correct;
	}
    public function getWrong() {
		return $this->wrong;
	}
    
	public function getPoints() {
		return $this->points;
	}
    
	public function getTime() {
		return $this->time;
	}
    
	public function getPercentile() {
		return $this->percentile;
	}
	public function getRank() {
		return $this->rank;
	}
    
    public function setSprite($x) {
		$this->sprite = $x;
	}
    public function getSprite() {
       return $this->sprite;
	}
    
    public function setColor($x) {
		$this->color = $x;
	}
    public function getColor() {
		return $this->color;
	}
}

?>