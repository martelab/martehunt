<?php

require_once 'log4php/LoggerManager.php';

class QuestConstraints
{
	private static $logger;
	private $id;
	private $user;	
	private $quest;

    private $settings;

  
    private $manualConstraint;
    private $manualSolved;
        
    private $posConstraint;
    private $latLon;
    private $maxDistance;
        
    private $lastUsed;
  
      
    public function __construct() {
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
        $this->manualConstraint = false;
        $this->posConstaint = true;
      
        $this->lastUsed = 0;
     
    }
  
   public function setId($x) {
		$this->id = $x;
	}
	
	public function getId() {
		return $this->id;
	}
  
	public function setSettings($x)
    {
      $this->settings = $x;
    }
  
	public function setUser($x) {
		$this->user = $x;
	}
	
	public function getUser() {
		return $this->user;
	}
	
  	public function setQuest($x) {
		$this->quest = $x;
	}
	
	public function getQuest() {
		return $this->quest;
	}
	
//////////////////MANUAL RELATED CONSTRAINTS
  
    
	public function setManualConstraint($x) { 
      if ($x == null) $this->manualConstraint = false;
      else if (is_string($x)) $this->manualConstraint = $x == "true";
      else $this->manualConstraint = $x;
	}

	
	public function isManualConstraint() {
		return $this->manualConstraint;
	}
  
  	public function setManualSolved($x) {
      if ($x == null) $this->manualSolved = false;
      else $this->manualSolved = $x;
	}
	
	public function isManualSolved() {
		return $this->manualSolved;
	}
	
	
    
//////////////////POSITION RELATED CONSTRAINTS
  
	public function setPosConstraint($x) {
      if ($x == null) $this->posConstraint = false;
      else if (is_string($x)) $this->posConstraint = $x == "true";
      else $this->posConstraint = $x;
	}
	
	public function isPosConstraint() {
		return $this->posConstraint;
	}
    
    public function setMaxDistance($x) {
		$this->maxDistance = floatval($x);
	}
	
	public function getMaxDistance() {
		return $this->maxDistance;
	}
     public function setLat($x) {
       if ($x != null){
		$this->latLon[0] = floatval($x);
        $this->posConstraint = true;
       }
	}
	
	public function setLon($x) {
       if ($x != null){
		$this->latLon[1] = floatval($x);
        $this->posConstraint = true;
       }
	}
  
    public function setLatLon($x) {
      if ($x != null){
		$this->latLon = $x;
        $this->posConstraint = true;
      } 
    }
	
	public function getLatLon() {
		return $this->latLon;
	}
  
  
//////////////////RANK RELATED CONSTRAINTS
  
    public function setLastUsed($x) {
      
		$this->lastUsed = $x;
	}
	
	public function getLastUsed() {
		return $this->lastUsed;
	}
  
   
	public function getRankTime($rank) {
      $MAX = $this->settings["rank"]["max"];
      $MAX_TIME = $this->settings["rank"]["delay"];

      if ($rank >= $MAX) return 0;
      $n = ($MAX - $rank)*$MAX_TIME/$MAX + strtotime($this->lastUsed);
      return $n;
	}
    
  
  
//////////////////CHECK LOGIC  
  
  
    public function check($t, $pos, $rank)
    {
        $check = [];
        $check["valid"] = true;
        
      if ($this->settings["rank"]["enabled"])
      {
        
        $rt = $this->getRankTime($rank);

        if ($t < $rt)
        {
            $check["valid"] = false;
            $check["time"] = $rt - $t;
        }
      }
      if ($this->settings["manual"]["enabled"] && $this->isManualConstraint())
      {
        if (!$this->isManualSolved()){

          $check["valid"] = false;
        }
        $check["manual"] = $this->isManualSolved();   
      }
      
      if ($this->settings["position"]["enabled"] && $this->isPosConstraint())
      {
            if (count($pos) < 2) $check["dist"] = INF;
            $dist = haversine_distance($pos, $this->getLatLon());

            if ($dist > $this->getMaxDistance())
            {
              $check["valid"] = false;
              $check["dist"] = $dist;
            }
      }
     
      return $check;
    }
}

?>