<?php

require_once 'log4php/LoggerManager.php';

class QuestData
{
	private static $logger;
	
	private $id;
	
	private $title;
	
	private $question;
    
    private $hint;
        
    private $answers;
    
    private $answerImage;
    private $hintImage;
    private $questionImage;
    
    
	public function __construct() {
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
	}
	
	public function setId($id) {
		$this->id = $id;
	}
	
	public function getId() {
		return $this->id;
	}
	
	public function setTitle($x) {
		$this->title = $x;
	}
	
	public function getTitle() {
		return $this->title;
	}
    
    public function setQuestion($x) {
        $this->question = $x;
    }
    
    public function getQuestion() {
        return $this->question;
    }
    
    public function setHint($hint) {
		$this->hint = $hint;
	}
	
	public function getHint() {
		return $this->hint;
	}
        
    public function setAnswers($arr) {   
		  $this->answer = $arr;
	}
	
	public function getAnswers() {
		return $this->answers;
	}
}

?>