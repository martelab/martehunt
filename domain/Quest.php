<?php

require_once 'log4php/LoggerManager.php';

class Quest
{
	private static $logger;
	
	private $id;
	
	private $name;
	
	private $secret;

    private $location;
    
    private $locationSecret;
    
    private $question;
    private $questionImage;
    
    private $answers;
    
	public function __construct() {
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
	}
	
	public function setId($id) {
		$this->id = $id;
	}
	
	public function getId() {
		return $this->id;
	}
    public function setLocation($id) {
		$this->location = $id;
	}
	
	public function getLocationSecret() {
		return $this->locationSecret;
	}
    
    public function setLocationSecret($s) {
		$this->locationSecret = $s;
	}
	
	public function getLocation() {
		return $this->location;
	}
    
	public function setName($name) {
		$this->name = $name;
	}
	
    public function getName() {
		return $this->name;
	}
    
	public function getQuestion() {
		return $this->question;
	}
    	public function getQuestionImage() {
		return $this->questionImage;
	}
    public function setQuestion($q) {
        
        $image = null;
        if (substr( $q, 0, 7 ) == "<image>"){
            $xml = simplexml_load_string($q);
            $this->questionImage = (string) $xml[0];
            $this->question = "";
        } else
        {
            $this->questionImage = null;
            $this->question = $q;
        }
	}
	
	public function setSecret($secret) {
		$this->secret = $secret;
	}
	
	public function getSecret() {
		return $this->secret;
	}
    
    public function setAnswers($arr) {   
		  $this->answers = $arr;
	}
	
	public function getAnswers() {
		return $this->answers;
	}
    
}

?>