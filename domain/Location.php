<?php

require_once 'log4php/LoggerManager.php';

class Location
{
	private static $logger;
	
	private $id;
	
	private $name;
	
    private $secret;
    
    private $hint;
	
    private $hintImage;
  
    private $type;
	
  public function __construct() {
		if (self::$logger == null) {
			self::$logger = & LoggerManager::getLogger(__CLASS__);
		}
	}
	
	public function setId($id) {
		$this->id = $id;
	}
	
	public function getId() {
		return $this->id;
	}
	
  	public function setType($x) {
		$this->type = $x;
	}
	
	public function getType() {
		return $this->type;
	}
	
  
	public function setName($name) {
		$this->name = $name;
	}
	
	public function getName() {
		return $this->name;
	}
	
    public function setSecret($secret) {
		$this->secret = $secret;
	}
	
	public function getSecret() {
		return $this->secret;
	}
            
    public function setHint($t) {
        $this->hint = $t;
      
       //check if the hint is an image
        if (substr( $t, 0, 7 ) == "<image>"){
            $xml = simplexml_load_string($t);
            $this->hintImage = (string) $xml[0];            
        } else $this->hintImage = null;
	}
	
	public function getHint() {
		return $this->hint;
	}
  
    public function getHintImage() {
		return $this->hintImage;
	}
}

?>