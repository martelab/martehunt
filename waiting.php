<?php
include 'global_init.php';

require_once 'utils.php';

//read start date from config file
$SETTINGS_PATH = INCLUDE_PATH . "/conf/config.xml";

$xml = simplexml_load_file($SETTINGS_PATH);

$s = $xml->xpath('dates')[0];

$dates = xml2array($s, true);

$template = $twig->loadTemplate('waiting.html');
       

// fill template with quest data and render
echo $template->render($dates);    

die();
            
?>