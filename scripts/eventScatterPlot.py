import pandas as pd
from datetime import datetime
import csv
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import numpy as np
import random


from matplotlib import rcParams
rcParams['font.family'] = 'sans-serif'
rcParams['font.sans-serif'] = ['DejaVu Sans',
                               'Lucida Grande', 'Verdana']
#rcParams['font.sans-serif'] = ['Tahoma']

f_size = 8
rcParams.update({'font.size': f_size})


def plot_n_save(sortdf, title, out_file):
    sortdf.name = pd.Categorical(sortdf.name)
    axes = sortdf.plot.barh()
    labels = list(pd.Categorical(sortdf.name))
    axes.set_yticklabels(labels, rotation=0)
    plt.title(title)
    axes.get_figure().set_size_inches(18.5, 10.5)
    plt.savefig(out_file,dpi=100)
    plt.show()




title = 'Santa Caterina 2019'
locationEventsPath = 'locationEvents2019.csv'
locationsStatsPath = 'locationstats2019.csv'
difficultyPath = 'locationstats_difficulty2019.csv'
correctPath = 'locationstats_answer2019.csv'

# < plot events > #############################################################à

headers = ['name','start','end', 'duration']
df = pd.read_csv(locationEventsPath,names=headers)

df.name = pd.Categorical(df.name)
print (df)
timestamps = list(df['start'].map(lambda x: datetime.strptime(str(x), '%Y-%m-%d %H:%M:%S').timestamp()))
#timestampsEnd = list(df['end'].map(lambda x: datetime.strptime(str(x), '%Y-%m-%d %H:%M:%S').timestamp())

labels = df['name']
codes =  list(map(int,df.name.cat.codes))

#csfont = {'fontname':'Arial'}
#matplotlib.rc('font', **csfont)

ax = plt.axes()
ax.xaxis.grid(True)
ax.set_axisbelow(True)

plt.scatter(timestamps, codes, c=codes, cmap='tab20' )

#plt.hold(True)
#plt.scatter(timestampsEnd, codes, c=codes, cmap='tab20' )

xt = np.linspace(timestamps[0], timestamps[-1], 30)

labelsx = map(lambda x : datetime.fromtimestamp(x +60*60).strftime("%H:%M") ,xt)
plt.xticks(xt,list(labelsx))
plt.yticks(codes, df.name)

# beautify the x-labels
plt.gcf().autofmt_xdate()
plt.title("Letture QR Code" + title)
plt.show()

# < plot histogram > ###########################################################
mpl_data = mdates.epoch2num(list(map(lambda x : x+2*60*60, timestamps)))

plt.figure(figsize=(20,10))
fig, ax = plt.subplots(1,1)
ax.hist(mpl_data, bins=30*4, color='lightblue',  edgecolor='blue', )
ax.xaxis.set_major_locator(mdates.HourLocator())
ax.xaxis.set_major_formatter(mdates.DateFormatter('%H.%M'))
fig.autofmt_xdate()
plt.title("Eventi " + title)
fig.set_size_inches(18.5, 10.5)
plt.savefig("events.png",dpi=100)


# < location stats>
headers = ['name','type','trovata', 'suggerita']
df = pd.read_csv(locationsStatsPath, names=headers)

df.name = pd.Categorical(df.name)

labels =  list(df.name)

print (df)

axes = df.plot.bar()
axes.set_xticklabels(labels, rotation=90)
plt.title("Dettagli QR code " + title)
plt.show()


# < location difficulty>
headers = ['name','domanda','suggerimento']
df = pd.read_csv(difficultyPath,names=headers)

df.name = pd.Categorical(df.name)

print (df)


sortdf = df.sort_values(by="domanda")
plot_n_save(sortdf, "Difficolta' QR code " + title, 'difficultyd.png')

sortdf = df.sort_values(by="suggerimento")
plot_n_save(sortdf, "Difficolta' QR code "+ title, 'difficultys.png')


# <location correct>
eaders = ['name','correct','total']
df = pd.read_csv(correctPath, names=headers)

df.name = pd.Categorical(df.name)

labels =  list(df.name)

print (df)
a = zip(list(df.correct), list(df.total) )
list(a)

percCorrect = list(map( lambda x : x[0]*100/x[1], list(a) ))

axes = plt.bar(df.correct /df.total)
df.plot.bar()
axes.set_xticklabels(labels, rotation=90)
plt.title("Dettagli QR code " + title)
plt.show()
