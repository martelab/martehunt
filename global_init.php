<?php
date_default_timezone_set("Europe/Rome");

if (! defined('INCLUDE_PATH')) {
	define('INCLUDE_PATH', dirname(__FILE__) . "");

    set_include_path(get_include_path() . PATH_SEPARATOR . dirname(__FILE__) . "/libs");
	set_include_path(get_include_path() . PATH_SEPARATOR . INCLUDE_PATH);
}

define('LOCALE_SUFFIX', 'it');

// set bundles
define('BUNDLE_PATH', INCLUDE_PATH . '/conf/properties');
define('DEFAULT_BUNDLE', BUNDLE_PATH . '/default.properties');
define('MAIL_BUNDLE', BUNDLE_PATH . '/mail.properties');

define('CONFIG_PATH', INCLUDE_PATH . '/conf/config.xml');
define('DAO_CONFIG_PATH', INCLUDE_PATH . '/conf/config.xml');
define('CACHE_CONFIG_PATH', INCLUDE_PATH . '/conf/cache-config.xml');
define('LOG4PHP_CONFIGURATION', INCLUDE_PATH . '/conf/log4php.xml');
define('LOGS_PATH', INCLUDE_PATH . '/logs');

//include Components auto loading
require __DIR__ . '/vendor/autoload.php';


// exception handler
require_once 'messages/ExceptionHandler.php';


//initalize templating

$templateLoader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($templateLoader, array(
    'cache' => 'templates/compilation_cache',
    'auto_reload' => true,
));
$twig->addExtension(new Twig_Extension_StringLoader());



// resoponse class
#require_once 'app/domain/client/Response.php';

////////// ENABLING ASSERTION
assert_options(ASSERT_ACTIVE, true);
assert_options(ASSERT_BAIL, true);

////////// ERROR HANDLING INITIALIZATION
#ExceptionHandler::init();

//////////global methods

// fill response with message provided by messenger
function fillMessages(Response $response, Messenger $messenger, User $user = null) {
	
	$messageConverter = new MessageConverter();
	if ($user != null) {
		$response->addMessages($messageConverter->convertToClientMessages(retrieveNotifications($user)));
	}
	
	// add execution messages
	$response->addMessages($messageConverter->convertToClientMessages($messenger->getMessages()));
	
	if (! $messenger->getMessages()->getMessagesOfType(MessageType::ERROR())->isEmpty()) {
		$response->setError(true);
	}

}

// fill response with message provided by messenger
// $user is provided only if service require authentication
function manageError(Response $response, Exception $e, User $user = null) {
	
	$messageConverter = new MessageConverter();
	
	if ($user != null) {
		$response->addMessages($messageConverter->convertToClientMessages(retrieveNotifications($user)));
	}
	
	$response->setError(true);
	
	if ($e instanceof MessageException) {
		$response->addMessage($messageConverter->convertToClientMessage($e->getMessage()));
	} else {
		$response->addMessage(MessageConverter::createDefaultErrorMessage());
	}

}

function retrieveNotifications(User $user) {
	
	/* @var $notificationService INotificationService */
	$notificationService = ServiceManager::getInstance(SERVICE_CONFIG_PATH)->getService('INotificationService');
	return $notificationService->retrieveMessages($user->getKey());

}

function authenticate($authKey) {
	
	/* @var $loginService ILoginService */
	$loginService = ServiceManager::getInstance(SERVICE_CONFIG_PATH)->getService('ILoginService');
	return $loginService->authenticate($authKey)->getUser();

}

?>