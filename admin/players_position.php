<?php

include 'admin_init.php';

require_once 'services.php';
require_once 'log4php/LoggerManager.php';
require_once 'db/DaoManager.php';



$xml = simplexml_load_file(CONFIG_PATH);
$googleapi = $xml->xpath('positioning/googleapi[1]')[0];

$logger = & LoggerManager::getLogger(basename(__FILE__, '.php'));

$template = $twig->loadTemplate('playerPosition.html');


  $daoManager = DaoManager::getInstance(DAO_CONFIG_PATH);

  $playerDao = $daoManager->getDao("Player");
 
  
  $players = $playerDao->selectByLastPosition();

 
echo $template->render(array("center" => $players[0], 
                             "players" => $players,
                              "googleApi"=> $googleapi
                               ));
