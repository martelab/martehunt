<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#000000">
    <meta http-equiv="cache-control" content="no-cache">

    <title>{{ title }}</title>

    <link rel="stylesheet" href="/grafica/pure/0.6.0/pure-min.css">
    <link rel="stylesheet" href="/grafica/font.css">
	<link rel="stylesheet" href="/grafica/martelab.css">
    <!--[if lte IE 8]>
        <link rel="stylesheet" href="grafica/pure/0.6.0/grids-responsive-old-ie-min.css">
    <![endif]-->
    <!--[if gt IE 8]><!-->
        <link rel="stylesheet" href="/grafica/pure/0.6.0/grids-responsive-min.css">
    <!--<![endif]-->
</head>


<body>

    <header class="pure-g">
        <div class="pure-u-1-4">
            <img class="pure-img" src="/grafica/img/logo-big.png" alt="Grande Caccia">
        </div>
    </header>
    <div class="pure-g">
    <div class="pure-menu pure-u-1-2">
    <span class="pure-menu-heading">Preparation actions</span>
    <ul class="pure-u-1-2 pure-menu-list">
        <li class="pure-menu-item"><a class="pure-menu-link" href="locations_list.php">Locations list</a></li>
        <li class="pure-menu-item"><a class="pure-menu-link" href="quests_list.php">Quests list</a></li>
        <li class="pure-menu-item"><a class="pure-menu-link" href="players_position.php">Player position</a></li>
    </ul>
    </div>
    <div class="pure-menu pure-u-1-2">
    <span class="pure-menu-heading">Live actions</span>
    <ul class="pure-u-1-2 pure-menu-list">
        <li class="pure-menu-item"><a class="pure-menu-link" href="manual_constraints.php">Manual constraints</a></li>
        <li class="pure-menu-item"><a class="pure-menu-link" href="../logs/log.php">Log</a></li>
    </ul>
   </div>

    </div>


    <br>  <br>  <br>  <br>
    <footer>
      <p id="debug" ></p>
        <p><a target="_blank" href="about.html">cos'è?</a></p>
        <p><a target="_blank" href="http://www.martelab.it">www.martelab.it</a></p>
    </footer>

</body>
</html>
