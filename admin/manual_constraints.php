<?php

include 'admin_init.php';

require_once 'services.php';
require_once 'log4php/LoggerManager.php';
require_once 'db/DaoManager.php';

$logger = & LoggerManager::getLogger(basename(__FILE__, '.php'));

$template = $twig->loadTemplate('constraint.html');
///post data
$name = null;
if($_POST && isset($_POST["id"])  && isset($_POST["location"]))
{
 $id = $_POST["id"];
 $locationName = $_POST["location"];

    

  $daoManager = DaoManager::getInstance(DAO_CONFIG_PATH);

  //1) get the user
  $playerDao = $daoManager->getDao("Player");
  $questDao = $daoManager->getDao("Quest");
  $constDao = $daoManager->getDao("QuestConstraints");

  
  $player = $playerDao->select_by_id_unsafe($id);


  if ($player == null) //stop here. No player registered
  {
      die("No player " . $id);
  }
  
  
  $quests = $questDao->selectByLocationName_and_player($locationName, $id);
  
  if ($quests == null || count($quests)==0) //stop here. No player registered
  {
      die("No active quests for location " . $locationName . " for user " .  $id . " ". $player->getName());
  }

  $logger->info("User " . $player->getName() . " " . $id . "completed manual constraint in ". $locationName );



  //select all quest of this location
  $count = 0;
  foreach($quests as $q)
  {
    
    $consts =  $constDao->select_by_quest($q->getId());

    foreach($consts as $c)
    { //TODO set only the one associated to the user?
      $count += $constDao->setCompleted($c->getId(), $player->getId());
    }
  }
  echo $template->render(array("player" => $player,
                                 "locationName"=>  $locationName,
                                  "done"=>($count > 0),
                                 
                                 
                                  )); 
   
}
else
{

  echo $template->render(array("done"=>false ));   
  
}
?>