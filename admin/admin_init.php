<?php
date_default_timezone_set("Europe/Rome");

if (! defined('INCLUDE_PATH')) {
	define('INCLUDE_PATH', dirname(__FILE__) . "/../" );
	set_include_path(get_include_path() . PATH_SEPARATOR . INCLUDE_PATH);
    set_include_path(get_include_path() . PATH_SEPARATOR . INCLUDE_PATH . "libs/");
}

define('LOCALE_SUFFIX', 'it');

// set bundles
define('BUNDLE_PATH', INCLUDE_PATH . '/conf/properties');
define('DEFAULT_BUNDLE', BUNDLE_PATH . '/default.properties');
define('MAIL_BUNDLE', BUNDLE_PATH . '/mail.properties');

define('CONFIG_PATH', INCLUDE_PATH . '/conf/config.xml');
define('DAO_CONFIG_PATH', INCLUDE_PATH . '/conf/config.xml');
define('CACHE_CONFIG_PATH', INCLUDE_PATH . '/conf/cache-config.xml');
define('LOG4PHP_CONFIGURATION', INCLUDE_PATH . '/conf/log4php.xml');
define('LOGS_PATH', INCLUDE_PATH . '/logs');

//include Components auto loading
require 'vendor/autoload.php';


// exception handler
require_once 'messages/ExceptionHandler.php';


//initalize templating

$templateLoader = new Twig_Loader_Filesystem(INCLUDE_PATH .'/templates');
$twig = new Twig_Environment($templateLoader, array(
    'cache' => INCLUDE_PATH .'/templates/compilation_cache',
    'auto_reload' => true,
));



// resoponse class
#require_once 'app/domain/client/Response.php';

////////// ENABLING ASSERTION
assert_options(ASSERT_ACTIVE, true);
assert_options(ASSERT_BAIL, true);

////////// ERROR HANDLING INITIALIZATION
#ExceptionHandler::init();

//////////global methods


?>