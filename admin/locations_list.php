<?php

include 'admin_init.php';

require_once 'log4php/LoggerManager.php';
require_once 'db/DaoManager.php';

$daoManager = DaoManager::getInstance(DAO_CONFIG_PATH);
$locationDao = $daoManager->getDao('Location');
///////////////////////////////////////
echo "<hr>";


echo "<h1>Location Quests</h1>";
$locations = $locationDao->selectAll();

echo "<p>" . sizeof($locations) . " locations present </p>";

foreach($locations as $l)
{
    echo "<p>";
    echo sprintf("location %u: %s - secret: %s <br>", $l->getId(), $l->getName(), $l->getSecret());
    echo sprintf("url: href=https://www.grandecaccia.it/quest-%s&-%s", $l->getId(), $l->getSecret());
 
    echo '<br><a href=https://www.grandecaccia.it/quest-'. $l->getId() .'-'.$l->getSecret().'>quest link</a>';
    echo ' | <a href=https://www.grandecaccia.it/quest-'. $l->getId() .'-'.$l->getSecret().'&a=a>answer link</a>';
 
    echo '<br><a href=../d.php?l='. $l->getId() .'&s='.$l->getSecret().'>local quest link</a>';
    echo '| <a href=../d.php?l='. $l->getId() .'&s='.$l->getSecret().'&a=a>local answer link</a>';
 
	echo "<br>QRCODE: <a href='https://barcode.tec-it.com/barcode.ashx?translate-esc=off&data=https%3A%2F%2Fwww.grandecaccia.it%2Fquest-". $l->getId()."-" .$l->getSecret() ."&code=QRCode&unit=Mm&dpi=96&imagetype=Gif&rotation=0&color=%23000000&bgcolor=%23ffffff&qunit=Mm&quiet=0&eclevel=L&modulewidth=50'/> https://www.grandecaccia.it/quest-" . $l->getId()."-" .$l->getSecret() . "</a>";
    
    echo "</p>";

    echo "<hr>";
}

echo "<p>";
echo "<h3>List of links</h3>";
foreach($locations as $l)
{
   echo sprintf("https://www.grandecaccia.it/quest-%s-%s<br>", $l->getId(), $l->getSecret());
}
echo "</p>";



echo "<h1>Quests Queries</h1>";
$questDao = $daoManager->getDao('Quest');
$quests = $questDao->selectAll();

echo "<p>" . sizeof($quests) . " quests present </p>";

foreach($quests as $quest)
{
    echo "<p>";
    echo sprintf("quest %u: %s location: %s - ", $quest->getLocation(), $quest->getName(), $quest->getLocation());       
    echo "</p>";
}
?>