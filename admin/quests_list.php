<?php

include 'admin_init.php';

require_once 'log4php/LoggerManager.php';
require_once 'db/DaoManager.php';
require_once 'services.php';

$daoManager = DaoManager::getInstance(DAO_CONFIG_PATH);
$questDao = $daoManager->getDao('Quest');
$quests = $questDao->selectAll();


$locationDao = $daoManager->getDao('Location');
$locations = $locationDao->selectAll();


$locationMap = array();
foreach($locations as $l)
{
  $locationMap[$l->getId()] = array("location" => $l, "quests" => array());
  
}

foreach($quests as $q)
{
   $q->setAnswers( prepareClientAnswersData($q->getAnswers(), array("valid" => true)));
    array_push($locationMap[$q->getLocation()]["quests"], $q);
 // var_dump($q);
}
//var_dump($locationMap);
///////////////////////////////////////

$template = $twig->loadTemplate('quest_admin.html');
       

// fill template with quest data and render
echo $template->render(array("data" => $locationMap));    
//
//
//
//echo "<hr>";
//
//echo "<h1>Quests Queries</h1>";
//
//
//echo "<p>" . sizeof($quests) . " quests present </p>";
//
//foreach($quests as $quest)
//{
//    echo "<p>";
//    echo sprintf("quest %u: %s location: %s <br>", $quest->getLocation(), $quest->getName(), $quest->getLocation());
//    //echo sprintf("hint: %s (%s.html)", $quest->getSecret(), $quest->getHintPage());
// 
//    echo '<br><a href=http://www.grandecaccia.it/quest-'. $quest->getLocation() .'-'.$quest->getLocationSecret().'>location link</a>';
//    echo ' | <a href=http://www.grandecaccia.it/quest-'. $quest->getLocation() .'-'.$quest->getLocationSecret().'&a=a>answer link</a>';
// 
//    echo '<br><a href=../d.php?l='. $quest->getLocation() .'&s='.$quest->getLocationSecret().'>local quest link</a>';
//    echo '| <a href=../d.php?l='. $quest->getLocation() .'&s='.$quest->getLocationSecret().'&a=a>local answer link</a>';
// 
//    echo "<br>";
//  
//  echo "<a href='https://barcode.tec-it.com/barcode.ashx?translate-esc=off&data=http%3A%2F%2Fgrandecaccia.it%2Fquest-". $quest->getLocation()."-" .$quest->getLocationSecret() ."&code=QRCode&unit=Mm&dpi=96&imagetype=Gif&rotation=0&color=%23000000&bgcolor=%23ffffff&qunit=Mm&quiet=0&eclevel=L&modulewidth=50'/>QRCODE: http://grandecaccia.it/d.php?l=1&s=et67qmo9zzky9kh9</a>";
//       
//     echo "<br>";
//    echo "<img height='128' width='128' src='https://barcode.tec-it.com/barcode.ashx?translate-esc=off&data=http%3A%2F%2Fgrandecaccia.it%2Fquest-". $quest->getLocation()."-" .$quest->getLocationSecret() ."&code=QRCode&unit=Mm&dpi=96&imagetype=Gif&rotation=0&color=%23000000&bgcolor=%23ffffff&qunit=Mm&quiet=0&eclevel=L&modulewidth=50'/>";
//    echo "</p>";
//
//    echo "<hr>";
//}
?>