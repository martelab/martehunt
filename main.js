/*global $*/
function pad2(number) {
    'use strict';
    return (number < 10 ? '0' : '') + number;
}

function cloneTemplate(queryClass) {
    'use strict';
    //get the corresponding template for this nodes (e.g group)
	var t, ni;
    t = $("#templates " + queryClass + ":first");
	if (t.length === 0) { return null; } //no template available

	ni = t.clone();
    return ni;
}

function resetRanking(div, data) {
    'use strict';
    var teamDiv = div.find(".teams");
    var playerDiv = div.find(".players");
    
    var teams = data.teamRank;
    var players = data.playerRank;

   // alert(JSON.stringify(data));
    teamDiv.html("");
    $.each(teams, function(idx, t){
        var ni = cloneTemplate(".team");
        ni.html(ni.html().replace(/_name_/g, t.name));
        ni.html(ni.html().replace(/_score_/g, t.score));
        ni.html(ni.html().replace(/_color_/g, t.color));

        teamDiv.append(ni);
    });

    playerDiv.html("");
    $.each(players, function(idx, p){
        var ni = cloneTemplate(".player");
        ni.html(ni.html().replace(/_name_/g, p.playerName));
        ni.html(ni.html().replace(/_points_/g, p.points));
        ni.html(ni.html().replace(/_teamName_/g, p.teamName));
        ni.html(ni.html().replace(/_color_/g, p.color));

        let sy = -20 - 91*Math.floor(p.sprite / 15);
        let sx = -14 - 36.4*Math.floor(p.sprite % 15);
        ni.html(ni.html().replace(/_sy_/g, sy));
        ni.html(ni.html().replace(/_sx_/g, sx));
        
        ni.html(ni.html().replace(/_odd_/g, idx % 2? "even" : "odd"));
        
        
        playerDiv.append(ni);
    });
}


function scaleSprite(d, s)
{
    let scaleProperties = [
            "height", 
            "width", 
            "background-position-x", 
            "background-position-y",
            "-webkit-mask-position-x",
            "-webkit-mask-position-y"];
    $.each(scaleProperties, function( id, prop ){
        let val = d.css(prop);
        d.css(prop, val*s);
        });
}

function makeSprites(div, inSprite, inColor, count) {

    for(let i=0; i < count; i++)
    {
        let x = $('<div class="sprite"></div>');

        let id = Math.floor(Math.random()*60);
        let alpha = Math.floor(Math.random()*180);
        let color = "#" + (Math.floor(Math.random()*255*255*255)).toString(16) + alpha.toString(16);

        let sy = -20 - 91*Math.floor(id / 15);
        let sx = -14 - 36.4*Math.floor(id % 15);

        x.css("background-color", color);
        x.css("background-position", sx + "px " + sy + "px");
        x.css("-webkit-mask-position", sx + "px " + sy + "px");
        x.appendTo(div);
        
        
        x.click(function() {
            let s = div.children(".selected");
                
            s.removeClass("selected");
            x.addClass("selected");
            
            s.animate({zoom : 1.0}, 100);
            x.animate({zoom : 2.0}, 100);
            s.css("-moz-transform", 'scale(1.0)');
            x.css("-moz-transform",  'scale(2.0)');
            
            inSprite.val(id);
            inColor.val(color);
        });
        
      
    }
}


function pollRanking(div) {
    'use strict';

    $.post('api/ranking.php', function (data) {
        resetRanking(div, data);
        
        setTimeout(pollRanking, 5000, div);
    });
}

function startTimer(theTime, dayNode, hourNode, minutesNode, secsNode) {
    const second = 1000,
      minute = second * 60,
      hour = minute * 60,
      day = hour * 24;
    
    let x = setInterval(function() {

      let now = new Date().getTime(),
          distance = theTime - now;
        
      if (distance < 0)
      {
       dayNode.html("00");
       hourNode.html("00");
       minutesNode.html("00");
       secsNode.html("00");
     }
     else
     {
       let d = Math.floor(distance / day)
       let h = Math.floor((distance % (day)) / (hour) )
       let m = Math.floor((distance % (hour)) / (minute) )
       let s  =Math.floor((distance % (minute)) / second);
         
       dayNode.html(d);
       hourNode.html(h);
       minutesNode.html(m);
       secsNode.html(s);
     }
     
    }, second);
    
}

function displayAnswers(divAnswers, data)
{
    divAnswers.html("");
    $.each(data, function(idx, t){
        var ni;
        if (t.image == null)
        {
          ni = cloneTemplate(".answer-text");
      
          ni.html(ni.html().replace(/[[color]]/g, t.color));

        }
        else
        {
          ni = cloneTemplate(".answer-image");
          ni.find("img").attr('src', t.image);
        }
        ni.html(ni.html().replace(/_name_/g, t.name));
        ni.html(ni.html().replace(/_text_/g, t.text));
        divAnswers.append(ni);
    });

}

function displayConstraints(divChecks, data)
{
   var distdiv =  divChecks.find("#distance")
    if (data.dist)
    {
       var v = Math.round(data.dist);
        distdiv.show();
        distdiv.find(".value").html( v  );
      if (v > 100) v = 100;
      distdiv.find("#distance_progress").width(v*60.0/100 + "%")
    } else 
    {
        distdiv.hide();
    }
    
    var timediv =  divChecks.find("#time")
    if (data.time)
    {
        timediv.show();
        timediv.data("countdown", data.time);

        var timef = function(t) 
        { //updats time as long as it is visible
          if (timediv.is(':visible') && t > 0 && t == timediv.data("countdown")){
            
            min = Math.floor(t / 60);
            sec = t - min * 60;
            timediv.find(".value").html( pad2(min) + ":" + pad2(sec) );
            timediv.data("countdown", t-1);
            setTimeout(function(){timef(t-1)}, 1000);
          }
        }
       timef(data.time);
    } else 
    {
        timediv.hide();
    }
}

function resetAnswers(divAnswers, divChecks, data)
{
  //$("#debug").html(JSON.stringify(data));
  
  if (data.length == 0)
  {
    window.location.replace("info.php");
  }
    if (data.answers)
    {
        //hide checks
        divChecks.hide();
        
        //display answer
        displayAnswers(divAnswers, data.answers );
        
        return 0;
    } 
    else 
    {  //update checks
      divAnswers.html("");
      divChecks.show();
      displayConstraints(divChecks, data);

      if (data.time) return data.time
      return 0;
    }
   
}

function pollQuestChecks($quest, divAnswers, divChecks) {
    //var gets = window.location.search.substr(1);
    var gets =  'l=' + $quest[0] + '&s=' + $quest[1];
    $.get('api/quest.php?' + gets, function (data) {
        var time = resetAnswers(divAnswers,divChecks, data);
        
        if (time > 0 && data.dist == null)
        {
            setTimeout(pollQuestChecks, time*1000, $quest, divAnswers, divChecks);
        } 
     });
}

    
    

     